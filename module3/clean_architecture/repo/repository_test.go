package repo

import (
	"io"
	"os"
	"testing"
)

func NewUserRepositoryTests(filepath string) io.ReadWriter {
	file, err := os.OpenFile(filepath, os.O_RDWR, 0666)
	if err != nil {
		return nil
	}
	defer file.Close()
	return io.ReadWriter(file)
}

func TestUserRepository_Save(t *testing.T) {
	//var r *UserRepository
	//t.Run("adding two positive numbers", func(t *testing.T) {
	//	_ = r.Save(User{
	//		ID:   1,
	//		Name: "SUIISS",
	//	})
	//	_ = r.Save(User{
	//		ID:   2,
	//		Name: "SUIISSSUIISS",
	//	})
	//	_ = r.Save(User{
	//		ID:   1,
	//		Name: "SUIISS",
	//	})
	//
	//})

	testCase := []struct {
		name     string
		argsUser User
		want     interface{}
	}{
		{
			name: "First",
			argsUser: User{
				ID:   1,
				Name: "SUIISS",
			},
			want: `[{"id":   1,"name": "SUIISS"},]`,
		},
		{
			name: "Second",
			argsUser: User{
				ID:   2,
				Name: "SUIISSSUIISS",
			},
			want: `[{"id":   1,"name": "SUIISS"},{"id":   2,"name": "SUIISSSUIISS"},]`,
		},
	}
	for _, tt := range testCase {
		t.Run(tt.name, func(t *testing.T) {
			if err := NewUserRepository(NewUserRepositoryTests("module3/clean_architecture/repo/test.json")).Save(tt.argsUser); err != nil {
				t.Errorf("SAVE error = %v, wantErr %v", err, tt.want)
			}
		})
	}
}

func TestUserRepository_Find(t *testing.T) {
	testCase := []struct {
		name   string
		argsID int
		want   interface{}
	}{
		{
			name:   "First",
			argsID: 1,
			want:   `[{"id":   1,"name": "SUIISS"},]`,
		},
		{
			name:   "Second",
			argsID: 2,
			want:   `[{"id":   2,"name": "SUIISSSUIISS"},]`,
		},
	}
	for _, tt := range testCase {
		t.Run(tt.name, func(t *testing.T) {
			if err, _ := NewUserRepository(NewUserRepositoryTests("module3/clean_architecture/repo/test.json")).Find(1); err != nil {
				t.Errorf("FIND error = %v, wantErr %v", err, tt.want)
			}
		})
	}
}

func TestUserRepository_FindAll(t *testing.T) {
	testCase := []struct {
		name string
		want interface{}
	}{
		{
			name: "Second",
			want: `[{"id":   1,"name": "SUIISS"},{"id":   2,"name": "SUIISSSUIISS"},]`,
		},
	}
	for _, tt := range testCase {
		t.Run(tt.name, func(t *testing.T) {
			if err, _ := NewUserRepository(NewUserRepositoryTests("module3/clean_architecture/repo/test.json")).FindAll(); err != nil {
				t.Errorf("SAVE error = %v, wantErr %v", err, tt.want)
			}
		})
	}
}
