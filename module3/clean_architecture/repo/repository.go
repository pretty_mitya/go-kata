package repo

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"os"
)

type Repository interface {
	Save(record interface{}) error
	Find(id int) (interface{}, error)
	FindAll() ([]interface{}, error)
}

type UserRepository struct {
	File io.ReadWriter
}

func NewUserRepository(file io.ReadWriter) *UserRepository {
	_, err := os.Create("test.json")
	if err != nil {
		log.Fatal(err)
	}
	file, err = os.OpenFile("module3/clean_architecture/repo/test.json", os.O_RDWR, 0666)
	if err != nil {
		return nil
	}
	return &UserRepository{
		File: file,
	}
}

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// Загружать данные в конструкторе

func (r *UserRepository) Save(record interface{}) error {
	// logic to write a record as a JSON object to a file at r.FilePath
	var users []User
	users = append(users, record.(User))
	file, err := json.MarshalIndent(users, "", " ")
	if err != nil {
		panic(err)
	}

	err = ioutil.WriteFile("test.json", file, 0)
	if err != nil {
		panic(err)
	}
	return nil
}

func (r *UserRepository) Find(id int) (interface{}, error) {
	// logic to read a JSON object from a file at r.FilePath and return the corresponding record
	users := []User{}
	for _, a := range users {
		if a.ID == id {
			return a, nil
		}
	}
	return nil, nil
}

func (r *UserRepository) FindAll() ([]interface{}, error) {
	// logic to read all JSON objects from a file at r.FilePath and return a slice of records
	var usersinter []interface{}
	var users []User
	for _, a := range users {
		usersinter = append(usersinter, a)
	}
	return usersinter, nil
}
