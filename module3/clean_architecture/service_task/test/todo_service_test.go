package test

import (
	"os"
	"reflect"
	"testing"

	"gitlab.com/pretty_mitya/go-kata/module3/clean_architecture/service_task/repo"
	"gitlab.com/pretty_mitya/go-kata/module3/clean_architecture/service_task/service"

	"gitlab.com/pretty_mitya/go-kata/module3/clean_architecture/service_task/model"
)

type TaskRepository interface {
	GetTasks() ([]model.Task, error)
	GetTask(id int) (model.Task, error)
	CreateTask(task model.Task) (model.Task, error)
	UpdateTask(task model.Task) (model.Task, error)
	DeleteTask(id int) error
}
type TodoService struct {
	repository TaskRepository
}

func NewService(t TaskRepository) *TodoService {
	return &TodoService{repository: t}
}

func TestNewService(t *testing.T) {
	file, _ := os.OpenFile("./test.json", os.O_CREATE|os.O_RDWR, 0644)
	repos := repo.NewRepo(file.Name())
	serv := service.NewService(repos)
	type args struct {
		t service.TaskRepository
	}
	tests := []struct {
		name string
		args args
		want *service.TodoService
	}{
		{
			name: "first",
			args: args{t: repos},
			want: serv,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := service.NewService(tt.args.t); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewService() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTodoService_CompleteTodo(t *testing.T) {
	file, _ := os.OpenFile("./test.json", os.O_CREATE|os.O_RDWR, 0644)
	repos := repo.NewRepo(file.Name())
	serv := service.NewService(repos)
	type args struct {
		id   int
		todo model.Task
	}
	tests := []struct {
		name    string
		fields  service.TodoServices
		args    args
		wantErr bool
	}{
		{
			name:   "first",
			fields: serv,
			args: args{
				id: 1,
				todo: model.Task{
					ID:          1,
					Title:       "first",
					Description: "first",
					Status:      "first",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &service.TodoService{
				repos,
			}
			if err := s.CompleteTodo(tt.args.id, tt.args.todo); (err != nil) != tt.wantErr {
				t.Errorf("CompleteTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestTodoService_CreateTodo(t *testing.T) {
	file, _ := os.OpenFile("./test.json", os.O_CREATE|os.O_RDWR, 0644)
	repos := repo.NewRepo(file.Name())
	type fields struct {
		repository service.TaskRepository
	}
	type args struct {
		id    int
		title string
		descr string
		stat  string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:   "first",
			fields: fields{repository: repos},
			args: args{
				id:    1,
				title: "first",
				descr: "first",
				stat:  "first",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &service.TodoService{
				Repository: tt.fields.repository,
			}
			if err := s.CreateTodo(tt.args.id, tt.args.title, tt.args.descr, tt.args.stat); (err != nil) != tt.wantErr {
				t.Errorf("CreateTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestTodoService_ListTodos(t *testing.T) {
	file, _ := os.OpenFile("./test.json", os.O_CREATE|os.O_RDWR, 0644)
	repos := repo.NewRepo(file.Name())
	type fields struct {
		repository service.TaskRepository
	}
	var tests = []struct {
		name    string
		fields  fields
		want    []model.Task
		wantErr bool
	}{
		{
			name:   "first",
			fields: fields{repos},
			want: []model.Task{
				{
					ID:          1,
					Title:       "first",
					Description: "first",
					Status:      "first",
				},
				{
					ID:          1,
					Title:       "first",
					Description: "first",
					Status:      "first",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &service.TodoService{
				Repository: tt.fields.repository,
			}
			_, err := s.ListTodos()
			if (err != nil) != tt.wantErr {
				t.Errorf("ListTodos() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestTodoService_ListTodosId(t *testing.T) {
	file, _ := os.OpenFile("./test.json", os.O_CREATE|os.O_RDWR, 0644)
	repos := repo.NewRepo(file.Name())
	type fields struct {
		repository service.TaskRepository
	}
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    model.Task
		wantErr bool
	}{
		{
			name:   "first",
			fields: fields{repos},
			args:   args{id: 1},
			want: model.Task{
				ID:          1,
				Title:       "first",
				Description: "first",
				Status:      "complete",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &service.TodoService{
				Repository: tt.fields.repository,
			}
			got, err := s.ListTodosId(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("ListTodosId() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ListTodosId() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTodoService_RemoveTodo(t *testing.T) {
	file, _ := os.OpenFile("./test.json", os.O_CREATE|os.O_RDWR, 0644)
	repos := repo.NewRepo(file.Name())
	type fields struct {
		repository service.TaskRepository
	}
	type args struct {
		todo model.Task
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:   "first",
			fields: fields{repository: repos},
			args: args{model.Task{
				ID:          4,
				Title:       "2",
				Description: "3",
				Status:      "4",
			}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &service.TodoService{
				Repository: tt.fields.repository,
			}
			if err := s.RemoveTodo(tt.args.todo); (err != nil) != tt.wantErr {
				t.Errorf("RemoveTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
