package test

import (
	"os"
	"reflect"
	"testing"

	repo2 "gitlab.com/pretty_mitya/go-kata/module3/clean_architecture/service_task/repo"

	"gitlab.com/pretty_mitya/go-kata/module3/clean_architecture/service_task/model"
)

type FileTaskRepository struct {
	FilePath string
}

func NewRepo(f string) *FileTaskRepository {
	return &FileTaskRepository{FilePath: f}
}

func TestFileTaskRepository_CreateTask(t *testing.T) {
	file, _ := os.OpenFile("./test.json", os.O_CREATE|os.O_RDWR, 0644)

	type fields struct {
		FilePath string
	}
	type args struct {
		task model.Task
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    model.Task
		wantErr bool
	}{
		{
			name: "first",
			fields: fields{
				file.Name(),
			},
			args: args{
				task: model.Task{
					ID:          1,
					Title:       "first",
					Description: "first",
					Status:      "first",
				},
			},
			want: model.Task{ID: 1,
				Title:       "first",
				Description: "first",
				Status:      "first",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo2.FileTaskRepository{
				FilePath: tt.fields.FilePath,
			}
			got, err := repo.CreateTask(tt.args.task)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateTask() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateTask() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFileTaskRepository_DeleteTask(t *testing.T) {
	file, _ := os.OpenFile("./test.json", os.O_CREATE|os.O_RDWR, 0644)

	type fields struct {
		FilePath string
	}
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "first",
			fields: fields{
				file.Name(),
			},
			args:    args{1},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo2.FileTaskRepository{
				FilePath: tt.fields.FilePath,
			}
			if err := repo.DeleteTask(tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("DeleteTask() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestFileTaskRepository_GetTask(t *testing.T) {
	file, _ := os.OpenFile("./test.json", os.O_CREATE|os.O_RDWR, 0644)

	type fields struct {
		FilePath string
	}
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    model.Task
		wantErr bool
	}{
		{
			name: "first",
			fields: fields{
				file.Name(),
			},
			args: args{1},
			want: model.Task{ID: 1,
				Title:       "first",
				Description: "first",
				Status:      "first",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo2.FileTaskRepository{
				FilePath: tt.fields.FilePath,
			}
			got, err := repo.GetTask(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetTask() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetTask() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFileTaskRepository_GetTasks(t *testing.T) {
	file, _ := os.OpenFile("./test.json", os.O_CREATE|os.O_RDWR, 0644)

	type fields struct {
		FilePath string
	}
	tests := []struct {
		name    string
		fields  fields
		want    []model.Task
		wantErr bool
	}{
		{
			name: "first",
			fields: fields{
				file.Name(),
			},
			want: []model.Task{{ID: 1,
				Title:       "first",
				Description: "first",
				Status:      "first",
			}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo2.FileTaskRepository{
				FilePath: tt.fields.FilePath,
			}
			got, err := repo.GetTasks()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetTasks() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetTasks() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFileTaskRepository_UpdateTask(t *testing.T) {
	file, _ := os.OpenFile("./test.json", os.O_CREATE|os.O_RDWR, 0644)

	type fields struct {
		FilePath string
	}
	type args struct {
		task model.Task
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    model.Task
		wantErr bool
	}{
		{
			name: "first",
			fields: fields{
				file.Name(),
			},
			args: args{
				task: model.Task{
					ID:          1,
					Title:       "first",
					Description: "first",
					Status:      "first",
				},
			},
			want: model.Task{ID: 1,
				Title:       "first",
				Description: "first",
				Status:      "first",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo2.FileTaskRepository{
				FilePath: tt.fields.FilePath,
			}
			got, err := repo.UpdateTask(tt.args.task)
			if (err != nil) != tt.wantErr {
				t.Errorf("UpdateTask() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UpdateTask() got = %v, want %v", got, tt.want)
			}
		})
	}
}
