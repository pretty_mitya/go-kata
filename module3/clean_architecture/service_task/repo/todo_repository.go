package repo

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/pretty_mitya/go-kata/module3/clean_architecture/service_task/model"
)

// Repository layer

//// Task represents a to-do task
//type model.Task struct {
//	ID          int    `json:"id"`
//	Title       string `json:"title"`
//	Description string `json:"description"`
//	Status      string `json:"status"`
//}

// TaskRepository is a repository interface for tasks
type TaskRepository interface {
	GetTasks() ([]model.Task, error)
	GetTask(id int) (model.Task, error)
	CreateTask(task model.Task) (model.Task, error)
	UpdateTask(task model.Task) (model.Task, error)
	DeleteTask(id int) error
	//InsertTask(id int, task model.Task) ([]model.Task, error)
}

// FileTaskRepository is a file-based implementation of TaskRepository
type FileTaskRepository struct {
	FilePath string
}

func NewRepo(f string) *FileTaskRepository {
	return &FileTaskRepository{FilePath: f}
}

// GetTasks returns all tasks from the repository --> DONE --> DONE
func (repo *FileTaskRepository) GetTasks() ([]model.Task, error) {
	var tasks []model.Task
	file, err := os.Open(repo.FilePath)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer file.Close()

	files, _ := os.ReadFile(file.Name())
	_ = json.Unmarshal(files, &tasks)

	return tasks, nil
}

// GetTask returns a single task by its ID --> DONE --> DONE
func (repo *FileTaskRepository) GetTask(id int) (model.Task, error) {
	var task model.Task
	tasks, err := repo.GetTasks()
	if err != nil {
		return task, err
	}

	for _, a := range tasks {
		if a.ID == id {
			return a, nil
		}
	}
	return task, nil
}

// CreateTask adds a new task to the repository --> DONE --> DONE
func (repo *FileTaskRepository) CreateTask(task model.Task) (model.Task, error) {
	tasks, err := repo.GetTasks()
	if err != nil {
		return task, err
	}
	tasks = append(tasks, task)

	file, err := json.MarshalIndent(tasks, "", " ")
	if err != nil {
		panic(err)
	}

	err = ioutil.WriteFile("./test.json", file, 0)
	if err != nil {
		panic(err)
	}

	return task, nil
}

// UpdateTask updates an existing task in the repository --> DONE --> DONE
func (repo *FileTaskRepository) UpdateTask(task model.Task) (model.Task, error) {
	tasks, err := repo.GetTasks()
	if err != nil {
		return task, err
	}

	for i, a := range tasks {
		switch {
		case a.ID == task.ID:
			tasks[i] = task
			//fmt.Println(tasks[i])
			break
		}
	}

	file, err := json.MarshalIndent(tasks, "", " ")
	if err != nil {
		panic(err)
	}

	err = ioutil.WriteFile("test.json", file, 0)
	if err != nil {
		panic(err)
	}

	return task, nil
}

// Delete id Task --> DONE --> DONE
func (repo *FileTaskRepository) DeleteTask(id int) error {
	tasks, err := repo.GetTasks()
	if err != nil {
		return err
	}
	//var deletedTask []model.Task
	for i, a := range tasks {
		switch {
		case len(tasks) <= 0:
			fmt.Println("empty model.Task")
		case a.ID == id:
			//deletedtasks = append(tasks[:i], tasks[i+1:]...)
			tasks = append(tasks[:i], tasks[i+1:]...)
			break
		}
	}
	file, err := json.MarshalIndent(tasks, "", " ")
	if err != nil {
		panic(err)
	}
	err = os.Truncate(string(file), 0)

	err = ioutil.WriteFile("./test.json", file, 0)
	if err != nil {
		panic(err)
	}
	return nil
}

//// Insert after id Task --> DONE
//func (repo *FileTaskRepository) InsertTask(id int, task model.Task) ([]model.Task, error) {
//	tasks, err := repo.GetTasks()
//	if err != nil {
//		return tasks, err
//	}
//	for i := 0; i < len(tasks); i++ {
//		switch {
//		case len(tasks) <= 0:
//			fmt.Println("empty model.Task")
//		case tasks[i].ID == id:
//			tasks = append(tasks, tasks[:i]...)
//			tasks = append(tasks, task)
//			tasks = append(tasks, tasks[i+1:]...)
//			break
//		}
//	}
//	file, err := json.MarshalIndent(tasks, "", " ")
//	if err != nil {
//		panic(err)
//	}
//	//if err := os.Truncate(string(file), 0); err != nil {
//	//	log.Printf("Failed to truncate: %v", err)
//	//}
//
//	err = ioutil.WriteFile("./test.json", file, 0)
//	if err != nil {
//		panic(err)
//	}
//	return tasks, nil
//}
