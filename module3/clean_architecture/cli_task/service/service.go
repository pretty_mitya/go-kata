package service

import (
	"gitlab.com/pretty_mitya/go-kata/module3/clean_architecture/cli_task/model"
)

type TodoServices interface {
	ListTodos() ([]model.Task, error)
	ListTodosId(id int) (model.Task, error)
	CreateTodo(id int, title string, descr string, stat string) error
	CompleteTodo(id int, todo model.Task) error
	RemoveTodo(todo model.Task) error
	//InsertTask(todo model.Task) error
}

type TaskRepository interface {
	GetTasks() ([]model.Task, error)
	GetTask(id int) (model.Task, error)
	CreateTask(task model.Task) (model.Task, error)
	UpdateTask(task model.Task) (model.Task, error)
	DeleteTask(id int) error
	//InsertTask(id int, task model.Task) ([]model.Task, error)
}

type TodoService struct {
	Repository TaskRepository
}

//func examToDo(r TaskRepository) (*TodoService, error) {
//	err := &TodoService{
//		repository: r,
//	}
//	if err != nil {
//		return nil, errors.New("error")
//	}
//	return err, nil
//}

func NewService(t TaskRepository) *TodoService {
	return &TodoService{Repository: t}
}

// все правильно
func (s *TodoService) ListTodos() ([]model.Task, error) {

	tasks, err := s.Repository.GetTasks()
	if err != nil {
		return nil, err
	}

	var todos []model.Task
	for _, task := range tasks {
		todos = append(todos, model.Task{
			ID:          task.ID,
			Title:       task.Title,
			Status:      task.Status,
			Description: task.Description,
		})
	}
	return todos, nil

}

// все правильно
func (s *TodoService) ListTodosId(id int) (model.Task, error) {
	a, _ := s.Repository.GetTask(id)
	return a, nil

}

// все правильно
func (s *TodoService) CreateTodo(id int, title string, descr string, stat string) error {

	s.Repository.CreateTask(model.Task{
		ID:          id,
		Title:       title,
		Description: descr,
		Status:      stat,
	})
	return nil
}

// все правильно
func (s *TodoService) CompleteTodo(id int, todo model.Task) error {
	_, err := s.Repository.UpdateTask(model.Task{
		ID:          id,
		Title:       todo.Title,
		Description: todo.Description,
		Status:      "complete",
	})
	if err != nil {
		return err
	}
	return nil
}

// все правильно
func (s *TodoService) RemoveTodo(todo model.Task) error {
	err := s.Repository.DeleteTask(todo.ID)
	if err != nil {
		return err
	}
	return nil
}

//func (s *TodoService) InsertTask(id int, todo model.Task) error {
//	_, err := s.repository.InsertTask(todo.ID, model.Task{
//		ID:          id + 1,
//		Title:       todo.Title,
//		Description: todo.Description,
//		Status:      todo.Status,
//	})
//	if err != nil {
//		return err
//	}
//	return nil
//}
