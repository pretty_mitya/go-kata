package main

import (
	"os"

	"gitlab.com/pretty_mitya/go-kata/module3/clean_architecture/cli_task/model"

	"gitlab.com/pretty_mitya/go-kata/module3/clean_architecture/cli_task/repo"
	"gitlab.com/pretty_mitya/go-kata/module3/clean_architecture/cli_task/service"
)

func main() {
	file, _ := os.OpenFile("./test.json", os.O_CREATE|os.O_RDWR, 0644)
	repos := repo.NewRepo(file.Name())
	serv := service.NewService(repos)
	//serv.CreateTodo(1, "2", "3", "4")
	//serv.CreateTodo(2, "2", "3", "4")
	//serv.CreateTodo(3, "2", "3", "4")
	//serv.CreateTodo(4, "2", "3", "4")
	serv.RemoveTodo(model.Task{ID: 1})

}
