package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"gitlab.com/pretty_mitya/go-kata/module3/clean_architecture/cli_task/model"

	repo "gitlab.com/pretty_mitya/go-kata/module3/clean_architecture/cli_task/repo"

	"gitlab.com/pretty_mitya/go-kata/module3/clean_architecture/cli_task/service"

	"github.com/AlecAivazis/survey/v2"
)

// StringPrompt asks for a string value using the label

func Checkboxes(label string, opts []string) []string {
	res := []string{}
	prompt := &survey.MultiSelect{
		Message: label,
		Options: opts,
	}
	survey.AskOne(prompt, &res)

	return res
}

func YesOrNo(opts []string) bool {
	res := false
	prompt := &survey.Confirm{
		Message: "Хочешь вернуться? (Yes/No)",
	}
	survey.AskOne(prompt, &res)

	return res
}
func nameStage(label string) string {
	var s string
	r := bufio.NewReader(os.Stdin)
	for {
		fmt.Fprint(os.Stderr, label+" ")
		s, _ = r.ReadString('\n')
		if s != "" {
			break
		}
	}
	return strings.TrimSpace(s)
}
func Executor(s string) {
	s = strings.TrimSpace(s)
	setCommand := strings.Split(s, " ")
	switch setCommand[0] {
	case "exit":
		fmt.Println("bye")
		os.Exit(0)
		return
	}
}

//	func completer(d prompt.Document) []prompt.Suggest {
//		var s []prompt.Suggest
//		switch d.Text {
//		case "Показать":
//			s = []prompt.Suggest{
//				{Text: "Показать список задач"},
//			}
//		}
//		return prompt.FilterHasPrefix(s, d.GetWordBeforeCursor(), true)
//	}
//func listOfTasks(serv *service.TodoService) {
//	tasks, err := serv.ListTodos()
//	if err != nil {
//		fmt.Println(errors.New("listOfTasks"))
//	}
//
//	for _, a := range tasks {
//		if len(tasks) <= 0 {
//			fmt.Println("No tasks, please add something")
//		} else {
//			fmt.Println(a)
//		}
//	}
//}

func main() {

	file, _ := os.OpenFile("./test.json", os.O_CREATE|os.O_RDWR, 0644)
	repo := repo.NewRepo(file.Name())
	serv := service.NewService(repo)

	name := nameStage("What is your name?")
	fmt.Printf("Hello, %s!\n", name)
Begin:
	answers := Checkboxes(
		"Что хочешь сделать?",
		[]string{
			"Показать список задач",
			"Добавить задачу",
			"Удалить задачу",
			"Показать задачу по ID",
			"Завершить задачу",
			"Exit",
		},
	)
	for _, a := range answers {
		switch {
		case a == "Показать список задач":
		Continue1:
			fmt.Println(serv.ListTodos())
			fmt.Println()
			if YesOrNo([]string{"Yes", "No"}) == true {
				goto Begin
			} else {
				fmt.Println("Посмотри тогда на список")
				goto Continue1
			}
		case a == "Добавить задачу":
			var (
				title, descr, s string
				id              int
			)
			fmt.Println("Введите Id, title, description, status через пробел ")
			fmt.Scanf("%d %s %s %s", &id, &title, &descr, &s)
			serv.CreateTodo(id, title, descr, s)
			fmt.Println("Задача создана")
			fmt.Println()
		Continue2:
			fmt.Println(serv.ListTodos())
			fmt.Println()
			if YesOrNo([]string{"Yes", "No"}) == true {
				goto Begin
			} else {
				fmt.Println("Посмотри тогда на список")
				goto Continue2
			}
		case a == "Удалить задачу":
			var (
				id int
			)
			fmt.Println("Вот кстати лист для удобства")
			fmt.Println(serv.ListTodos())
			fmt.Println()
			fmt.Println("Введите Id")
			fmt.Scanf("%d", &id)
			serv.RemoveTodo(model.Task{ID: id})
			fmt.Println("Задача удалена")
			fmt.Println("Список задач")
		Continue3:
			fmt.Println(serv.ListTodos())
			fmt.Println()
			if YesOrNo([]string{"Yes", "No"}) == true {
				goto Begin
			} else {
				fmt.Println("Посмотри тогда на список")
				goto Continue3
			}
		case a == "Показать задачу по ID":
		Continue4:
			var id int
			fmt.Println("Введите Id")
			fmt.Scanf("%d", &id)
			fmt.Println(serv.ListTodosId(id))
			fmt.Println()
			if YesOrNo([]string{"Yes", "No"}) == true {
				goto Begin
			} else {
				fmt.Println("Посмотри тогда на список")
				goto Continue4
			}
		case a == "Завершить задачу":
		Continue5:
			var (
				title, descr string
				id           int
			)
			fmt.Println("Посмотрите на task, который можно complete")
			fmt.Println()
			fmt.Println(serv.ListTodos())
			fmt.Println()
			fmt.Println("Введите Id, title, description этого TASK через пробел ")
			fmt.Scanf("%d %s %s %s", &id, &title, &descr)
			fmt.Println(serv.CompleteTodo(id, model.Task{
				ID:          id,
				Title:       title,
				Description: descr,
			}))
			fmt.Println()
			fmt.Println("Теперь задача имеет статус complete")
			fmt.Println()
			if YesOrNo([]string{"Yes", "No"}) == true {
				goto Begin
			} else {
				fmt.Println("Посмотри тогда на список")
				goto Continue5
			}
		case a == "Exit":
		}
	}
	fmt.Println("Вы нечаянно или специально нажали Enter/Exit, ждем вас еще")
}
