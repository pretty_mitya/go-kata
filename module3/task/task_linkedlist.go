package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"time"

	"github.com/brianvoe/gofakeit/v6"
)

type DoubleLinkedList struct {
	head *Node // начальный элемент в списке
	tail *Node // последний элемент в списке
	curr *Node // текущий элемент меняется при использовании методов next, prev
	len  int   // количество элементов в списке
}
type Node struct {
	data *Commit
	prev *Node
	next *Node
}

type Commit struct {
	Message string    `json:"message"`
	UUID    string    `json:"uuid"`
	Date    time.Time `json:"date"`
}

var path = "https://gist.githubusercontent.com/ptflp/5464333906b11d38168c0a4507f862e4/raw/2692fa8612fcac88c1e47fddef253097e9a0aacb/test.json"

type LinkedLister interface {
	Len() int                              //+
	Current() (*Node, error)               //+
	Next() (*Node, error)                  //+
	Prev() (*Node, error)                  //+
	LoadData(path string) error            //+
	Insert(n int, c Commit) error          //+
	Delete(n int) error                    //+
	DeleteCurrent() error                  //+
	Index() (int, error)                   //+
	Pop() (*Node, error)                   //+
	Shift() (*Node, error)                 //+
	SearchUUID(uuID string) (*Node, error) //+
	Search(message string) (*Node, error)  //+
	Reverse() *DoubleLinkedList            //+
}

// LoadData загрузка данных из подготовленного json файла
func (d *DoubleLinkedList) LoadData(path string) error {
	var mainStructure []Commit
	siteHTTP, err := http.Get(path)
	if err != nil {
		panic(err)
	}
	defer siteHTTP.Body.Close()
	jsonfile, err := ioutil.ReadAll(siteHTTP.Body)
	if err != nil {
		panic(err)
	}
	//isvalid := json.Valid(jsonfile)
	//fmt.Println("Json Valid? ->", isvalid)
	// отсортировать список используя самописный QuickSort
	jsonerr := json.Unmarshal(jsonfile, &mainStructure)
	if jsonerr != nil {
		log.Fatal(jsonerr)
	}

	sortedstructure := QuickSort(mainStructure)
	for _, i := range sortedstructure {
		mainNode := &Node{
			data: &i,
			prev: nil,
			next: nil,
		}
		if d.head == nil {
			d.head = mainNode
			d.curr = mainNode
			d.tail = mainNode
		} else {
			d.tail.next = mainNode
			mainNode.prev = d.tail
			d.tail = mainNode
			d.curr = mainNode
		}
		d.len++
	}
	//fmt.Println(sortedstructure)

	return nil
}

func QuickSort(mainStructure []Commit) []Commit {

	if len(mainStructure) <= 1 {
		return mainStructure
	}
	left_part := make([]Commit, 0, len(mainStructure))
	right_part := make([]Commit, 0, len(mainStructure))
	middle_part := make([]Commit, 0, len(mainStructure))

	center := rand.Intn(len(mainStructure))

	for i := range mainStructure {
		if mainStructure[i].Date.Unix() < mainStructure[center].Date.Unix() {
			left_part = append(left_part, mainStructure[i])
		} else if mainStructure[i].Date.Unix() > mainStructure[center].Date.Unix() {
			right_part = append(right_part, mainStructure[i])
		} else if mainStructure[i].Date.Unix() == mainStructure[center].Date.Unix() {
			middle_part = append(middle_part, mainStructure[i])
		}
	}
	left_part = QuickSort(left_part)
	right_part = QuickSort(right_part)

	left_part = append(left_part, middle_part...)
	left_part = append(left_part, right_part...)

	return left_part
}

// Len получение длины списка
func (d *DoubleLinkedList) Len() int {
	return d.len
}

// Current получение текущего элемента
func (d *DoubleLinkedList) Current() (*Node, error) {
	var err error
	if d.head == nil {
		return nil, err
	} else if d.len == 0 {
		return nil, errors.New("err2")
	} else if d.curr.prev == nil {
		return nil, errors.New("err3")
	}
	return d.curr, nil
}

// Next получение следующего элемента
func (d *DoubleLinkedList) Next() (*Node, error) {
	var err error
	if d.head == nil {
		return nil, err
	} else if d.len == 0 {
		return nil, errors.New("err2")
	} else if d.curr.prev == nil {
		return nil, errors.New("err3")
	}
	currentPost := d.curr
	return currentPost.next, nil
}

// Prev получение предыдущего элемента
func (d *DoubleLinkedList) Prev() (*Node, error) {
	var err error
	if d.head == nil {
		return nil, err
	} else if d.len == 0 {
		return nil, errors.New("err2")
	} else if d.curr.prev == nil {
		return nil, errors.New("err3")
	}
	currentPost := d.curr
	return currentPost.prev, nil
}

// Insert вставка элемента после n элемента
func (d *DoubleLinkedList) Insert(n int, c Commit) error {
	newNode := &Node{
		data: &c,
		prev: nil,
		next: nil,
	}
	if n == 0 {
		if d.head == nil {
			d.head = newNode
			d.curr = newNode
			d.tail = newNode
			d.len++
			return nil
		} else {
			newNode2 := d.head
			d.head = newNode
			newNode.next = newNode2
			d.len++
			return nil
		}
	} else if n == d.len-1 {
		if d.head == nil {
			d.head = newNode
			d.curr = newNode
			d.tail = newNode
			d.len++
			return nil
		} else {
			newNode2 := d.head
			for newNode2.next != nil {
				newNode2 = newNode2.next
			}
			newNode2.next = newNode
			d.len++
			return nil
		}
	} else {
		newNode2 := d.head
		for i := 0; i < n-1; i++ {
			newNode2 = newNode2
		}
		newNode.next = newNode2.next
		newNode2.next = newNode
		d.len++
	}

	return nil
}

// Delete удаление n элемента
func (d *DoubleLinkedList) Delete(n int) error {
	if n == 0 {
		fmt.Println("--->n must me positive and not nil")
		return nil
	} else if d.len == 1 {
		d.head = nil
		d.curr = nil
		d.tail = nil
		d.len--
		return nil
	} else if n >= d.len {
		fmt.Println("--->out of list")
		return nil
	} else {
		for i := 0; i < d.len; i++ {
			if i == n-1 {
				nextnode := d.head.next
				d.head.next = nextnode.next
				nextnode.next = nil
				if n == d.len-1 {
					d.tail = d.head
				}
				break
			}
		}
		d.head = d.head.next
	}
	d.len--
	return nil
}

// DeleteCurrent удаление текущего элемента
func (d *DoubleLinkedList) DeleteCurrent() error {
	if d.len == 1 {
		d.head = nil
		d.curr = nil
		d.tail = nil
		d.len--
		return nil
	} else if d.len < 1 {
		fmt.Println("--->out of list")
		return nil
	} else {
		return nil
	}
}

// Index получение индекса текущего элемента
func (d *DoubleLinkedList) Index() (int, error) {
	currentIndex := 0
	currentPost := d.head

	for currentIndex < d.len {
		if currentPost == d.curr {
			return currentIndex, nil
		}
		currentPost = currentPost.next
		currentIndex++
	}
	return currentIndex, fmt.Errorf("no index")
}

// Pop Операция Pop
func (d *DoubleLinkedList) Pop() (*Node, error) {
	//удаление последнего элемента
	if d.len == 0 {
		return nil, nil
	}
	temptail := d.tail
	if d.head.next == nil {
		d.head = nil
		d.tail = nil
		d.len--
		return temptail, nil
	}
	currnode := d.head
	for currnode.next != nil {
		if currnode.next.next == nil {
			currnode.next = nil
		} else {
			currnode = currnode.next
		}
	}
	d.len--
	d.tail = currnode
	return temptail, nil
}

// Shift операция shift
func (d *DoubleLinkedList) Shift() (*Node, error) {
	if d.head == nil {
		return nil, nil
	}
	temphead := d.head

	if d.head.next != nil {
		d.head = d.head.next
	} else {
		d.head = nil
	}
	d.len--

	return temphead, nil
}

// SearchUUID поиск коммита по uuid
func (d *DoubleLinkedList) SearchUUID(uuID string) (*Node, error) {
	if d.head == nil {
		panic("empty")
	}
	searchMess := d.head
	for i := 0; i < d.len; i++ {
		if searchMess.data.UUID == uuID {
			return searchMess, nil
		}
		searchMess = searchMess.next
	}
	return searchMess, nil
}

// Search поиск коммита по message
func (d *DoubleLinkedList) Search(message string) (*Node, error) {
	searchMess := d.head
	for i := 0; i < d.len; i++ {
		if searchMess.data.Message == message {
			return searchMess, nil
		}
		searchMess = searchMess.next
	}
	return searchMess, nil
}

// Reverse возвращает перевернутый список
func (d *DoubleLinkedList) Reverse() (*DoubleLinkedList, error) {
	var cursor *Node = d.head
	var prev *Node = nil

	for cursor != nil {
		var next = cursor.next
		cursor.next = prev
		prev = cursor
		cursor = next
	}
	d.head = prev
	return d, nil
}

func GenerateJSON() string {
	// Дополнительное задание написать генератор данных
	// используя библиотеку gofakeit
	structJSON := []Commit{}
	rand.Seed(time.Now().UnixNano())
	newStruct := &Commit{
		Message: gofakeit.Sentence(rand.Intn(10)),
		UUID:    gofakeit.UUID(),
		Date:    gofakeit.Date(),
	}
	for i := 0; i < rand.Intn(10); i++ {
		structJSON = append(structJSON, *newStruct)
	}
	datajson, err := json.Marshal(structJSON)
	if err != nil {
		log.Fatalf("JSON marshaling failed: %s\"", err)
	}
	return string(datajson)
}

func main() {
	d := &DoubleLinkedList{}
	downloads := d.LoadData(path)
	if downloads != nil {
		panic(downloads)
	}

}
