package main

import (
	"fmt"
	"reflect"
	"testing"
	"time"
)

var (
	mainStructure []Commit
)

func maintests() *DoubleLinkedList {
	d := &DoubleLinkedList{}
	download := d.LoadData(path)
	_ = download
	return d
}
func TestDoubleLinkedList_LoadData(t *testing.T) {
	type args struct {
		path string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Successful Download",
			args: args{
				path: path,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{}
			if err := d.LoadData(tt.args.path); (err != nil) != tt.wantErr {
				t.Errorf("LoadData() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func dataTest(structura []Commit) []Commit {
	for i := 0; i < len(structura); i++ {
		for j := 0; j < len(structura); j++ {
			if structura[i].Date.Unix() < structura[j].Date.Unix() {
				structura[i], structura[j] = structura[j], structura[i]
			}
		}
	}
	return structura
}

func TestQuickSort(t *testing.T) {
	type args struct {
		firstStructure []Commit
	}
	tests := []struct {
		name string
		args args
		want []Commit
	}{
		{
			name: "QuickSort == Simple Sort by date",
			args: args{
				firstStructure: mainStructure,
			},
			want: dataTest(mainStructure),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := QuickSort(tt.args.firstStructure); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("QuickSort1() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Len(t *testing.T) {
	testsofthistask := []struct {
		name  string
		dlina int
		want  int
	}{
		{
			name:  "len",
			dlina: maintests().Len(),
			want:  2000,
		},
	}
	for _, tt := range testsofthistask {
		t.Run(tt.name, func(t *testing.T) {
			err := tt.want
			if err != tt.dlina {
				t.Errorf("In the '%v' we see '%v' and want '%v'", tt.name, tt.dlina, tt.want)
			} else {
				fmt.Println("Good job my friend, LEN COMPLETED")
			}

		})
	}
}

func TestDoubleLinkedList_Current(t *testing.T) {
	ty, err := maintests().Current()
	type args struct {
		curr *Node
		err  error
	}
	testsofthistask := []struct {
		name    string
		cur     args
		wanterr error
	}{
		{
			name: "current",
			cur: args{
				curr: ty,
				err:  err,
			},
			wanterr: nil,
		},
	}
	for _, tt := range testsofthistask {
		t.Run(tt.name, func(t *testing.T) {
			_, err1 := maintests().Next()
			if err1 != tt.wanterr {
				t.Errorf("In the '%v' we see '%v' and want '%v'", tt.name, tt.cur, tt.wanterr)
			} else {
				fmt.Println("Good job my friend, CURRENT COMPLETED")
			}

		})
	}
}

func TestDoubleLinkedList_Next(t *testing.T) {
	var err error
	ty, _ := maintests().Next()
	type args struct {
		prev *Node
		err  error
	}
	testsofthistask := []struct {
		name    string
		next    args
		wanterr error
	}{
		{
			name: "next",
			next: args{
				prev: ty,
				err:  err,
			},
			wanterr: nil,
		},
	}
	for _, tt := range testsofthistask {
		t.Run(tt.name, func(t *testing.T) {
			_, err1 := maintests().Next()
			if err1 != tt.wanterr {
				t.Errorf("In the '%v' we see '%v' and want bool '%v'", tt.name, tt.next, tt.wanterr)
			} else {
				fmt.Println("Good job my friend, NEXT COMPLETED")
			}

		})
	}
}

func TestDoubleLinkedList_Prev(t *testing.T) {
	var err error
	ty, e := maintests().Prev()
	type args struct {
		prev *Node
		err  error
	}
	testsofthistask := []struct {
		name    string
		prev    args
		wanterr error
	}{
		{
			name: "test",
			prev: args{
				prev: ty,
				err:  e,
			},
			wanterr: nil,
		},
	}
	for _, tt := range testsofthistask {
		t.Run(tt.name, func(t *testing.T) {
			_, err1 := maintests().Prev()
			if err1 != tt.wanterr {
				t.Errorf("In the '%v' we see '%v' and want bool '%v'", tt.name, tt.prev, err)
			} else {
				fmt.Println("Good job my friend, PREV COMPLETED")
			}
		})
	}
}

func TestDoubleLinkedList_Insert(t *testing.T) {
	type args struct {
		n int
		c *DoubleLinkedList
	}
	testsofthistask := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "minus",
			args: args{
				n: -1,
				c: maintests(),
			},
			wantErr: true,
		},
		{
			name: "nil",
			args: args{
				n: 0,
				c: maintests(),
			},
			wantErr: false,
		},
		{
			name: "plus",
			args: args{
				n: 2,
				c: maintests(),
			},
			wantErr: false,
		},
	}
	for _, tt := range testsofthistask {
		t.Run(tt.name, func(t *testing.T) {
			err := tt.args.c.Insert(tt.args.n, Commit{
				Message: "commit",
				UUID:    "123",
				Date:    time.Time{},
			})
			if err != nil {
				t.Errorf("In the %v we see %v", tt.name, tt.wantErr)
			} else {
				fmt.Println("Good job my friend, INSERT COMPLETED")
			}
		})
	}
}

func TestDoubleLinkedList_Delete(t *testing.T) {
	type args struct {
		n int
		c *DoubleLinkedList
	}
	testsofthistask := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name:    "incorrect test1",
			args:    args{n: -1, c: maintests()},
			wantErr: true,
		},
		{
			name:    "incorrect test2",
			args:    args{n: 0, c: maintests()},
			wantErr: true,
		},
		{
			name:    "incorrect test3",
			args:    args{n: 2001, c: maintests()},
			wantErr: false,
		},
		{
			name:    "correct test",
			args:    args{n: 2, c: maintests()},
			wantErr: false,
		},
	}
	for _, tt := range testsofthistask {
		t.Run(tt.name, func(t *testing.T) {
			err := tt.args.c.Delete(tt.args.n)
			if err != nil {
				t.Errorf("In the '%v' we see '%v'", tt.name, tt.wantErr)
			} else {
				fmt.Println("Good job my friend, DELETE COMPLETED")
			}
		})
	}
}

func TestDoubleLinkedList_DeleteCurrent(t *testing.T) {
	testsofthistask := []struct {
		name    string
		dl      *DoubleLinkedList
		wantErr error
	}{
		{
			name:    "correct test",
			dl:      maintests(),
			wantErr: nil,
		},
		{
			name:    "empty incorrect test",
			dl:      new(DoubleLinkedList),
			wantErr: fmt.Errorf("empty"),
		},
	}
	for _, tt := range testsofthistask {
		t.Run(tt.name, func(t *testing.T) {
			err := tt.dl.DeleteCurrent()
			if err != nil {
				t.Errorf("In the '%v' we see '%v'", tt.name, tt.wantErr)
			} else {
				fmt.Println("Good job my friend, DELETECURRENT COMPLETED")
			}
		})
	}
}

func TestDoubleLinkedList_Index(t *testing.T) {
	ty, err := maintests().Next()
	type args struct {
		index *Node
		err   error
	}
	tests := []struct {
		name    string
		ind     args
		wanterr error
	}{
		{
			name: "test",
			ind: args{
				index: ty,
				err:   err,
			},
			wanterr: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := maintests().Index()
			if err != tt.wanterr {
				t.Errorf("Index() error = %v, wantErr %v", err, tt.wanterr)
			} else {
				fmt.Println("Good")
			}
		})
	}
}

func TestDoubleLinkedList_SearchUUID(t *testing.T) {
	uuid := Node{
		data: &Commit{
			Message: "",
			UUID:    "6958701a-875b-11ed-8150-acde48001122",
			Date:    time.Time{},
		},
	}
	type uuuuuu struct {
		uuID *Node
		err  error
	}
	ty, err1 := maintests().SearchUUID(uuid.data.UUID)
	tests := []struct {
		name    string
		Uuuuuu  uuuuuu
		wanterr error
	}{
		{
			name: "searchUUID correct",
			Uuuuuu: uuuuuu{
				uuID: ty,
				err:  err1,
			},
			wanterr: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err2 := maintests().SearchUUID(uuid.data.UUID)
			if err2 != tt.wanterr {
				t.Errorf("In the '%v' we see '%v' and want bool '%v'", tt.name, tt.Uuuuuu, tt.wanterr)
			} else {
				fmt.Println("Good job my friend, SEARCHUUID COMPLETED")
			}

		})
	}
}

func TestDoubleLinkedList_Search(t *testing.T) {
	a := "You can't generate the array without programming the auxiliary USB monitor!"
	ty, e := maintests().Search(a)
	type args struct {
		uuID *Node
		err  error
	}
	tests := []struct {
		name    string
		args    args
		wantErr error
	}{
		{
			name: "search correct",
			args: args{
				uuID: ty,
				err:  e,
			},
			wantErr: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := maintests().Search(a)
			if err != tt.wantErr {
				t.Errorf("In the '%v' we see '%v' and want bool '%v'", tt.name, tt.args, tt.wantErr)
			} else {
				fmt.Println("Good job my friend, SEARCH COMPLETED")
			}

		})
	}
}

func TestDoubleLinkedList_Pop(t *testing.T) {
	tests := []struct {
		name    string
		args    *DoubleLinkedList
		wantErr error
	}{
		{
			name:    "empty test",
			args:    new(DoubleLinkedList),
			wantErr: fmt.Errorf("empty"),
		},
		{
			name:    "test",
			args:    maintests(),
			wantErr: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := tt.args.Pop()
			if err != nil {
				t.Errorf("%v", tt.wantErr)
			} else {
				fmt.Println("Good job my friend, POP COMPLETED")
			}
		})
	}
}

func TestDoubleLinkedList_Shift(t *testing.T) {
	tests := []struct {
		name    string
		dl      *DoubleLinkedList
		wantErr error
	}{
		{
			name:    "empty incorrect test",
			dl:      new(DoubleLinkedList),
			wantErr: fmt.Errorf("empty"),
		},
		{
			name:    "test",
			dl:      maintests(),
			wantErr: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := tt.dl.Shift()
			if err != nil {
				t.Errorf("%v", tt.dl)
			} else {
				fmt.Println("Good job my friend, SHIFT COMPLETED")
			}
		})
	}
}

func TestDoubleLinkedList_Reverse(t *testing.T) {
	tests := []struct {
		name    string
		dl      *DoubleLinkedList
		wantErr error
	}{
		{
			name:    "empty test",
			dl:      new(DoubleLinkedList),
			wantErr: fmt.Errorf("empty"),
		},
		{
			name:    "test",
			dl:      maintests(),
			wantErr: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := tt.dl.Reverse()
			if err != nil {
				t.Errorf("%v", err)
			} else {
				fmt.Println("Good job my friend, REVERSE COMPLETED")
			}
		})
	}
}
