package main

import "fmt"

type Navigator struct {
	PricingStrategy
}

func (nav *Navigator) PriceStrategy(str PricingStrategy) {
	nav.PricingStrategy = str
}

type PricingStrategy interface {
	Calculate(order Order) float64
}

type RegularPricing struct {
	firstPrice float64
}

func (c *RegularPricing) Calculate(order Order) float64 {
	if order.productPrice > 0 {
		c.firstPrice = order.productPrice * float64(order.productCount)
	} else {
		panic("<= 0")
	}
	return c.firstPrice
}

type SalePricing struct {
	name      string
	salePrice float64
}

func (c *SalePricing) Calculate(order Order) float64 {
	products := order.productPrice * float64(order.productCount)
	if c.salePrice >= 20 {
		endprice := products - products*(c.salePrice/100)
		return endprice
	} else if c.salePrice < 20 && c.salePrice > 10 {
		c.name = "Весенняя скидка 15% только сегодня"
		endprice := products - products*(c.salePrice/100)
		return endprice
	} else if c.salePrice > 0 && c.salePrice <= 10 {
		c.name = "Скидка за улыбку"
		endprice := products - products*(c.salePrice/100)
		return endprice
	} else {
		return products
	}
}

type Order struct {
	productName  string
	productPrice float64
	productCount int
}

func (o Order) Calculate(order Order) float64 {
	return order.productPrice
}

func main() {
	nav := Navigator{}

	sliceofinterfaces := []PricingStrategy{
		&RegularPricing{}, &SalePricing{"10", 10},
		&RegularPricing{}, &SalePricing{"15", 15},
		&RegularPricing{}, &SalePricing{"20", 20},
	}
	order := Order{
		productName:  "order",
		productPrice: 1000,
		productCount: 2,
	}
	order1 := Order{
		productName:  "order",
		productPrice: 0,
		productCount: 2,
	}

	for i, strategy := range sliceofinterfaces {
		nav.PriceStrategy(strategy)
		nav.Calculate(order)
		nav.Calculate(order1)
		fmt.Printf("Total cost with %T strategy: %v \n", sliceofinterfaces[i], nav.Calculate(order))
		//panic down
		//fmt.Printf("Total cost with %T strategy: %v \n", sliceofinterfaces[i], nav.Calculate(order1))

	}
}
