package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

// WeatherAPI is the interface that defines the methods for accessing weather information
type WeatherAPI interface {
	GetTemperature(location string) interface{}
	GetHumidity(location string) interface{}
	GetWindSpeed(location string) interface{}
}

// OpenWeatherAPI is the implementation of the weather API
type OpenWeatherAPI struct {
	apiKey string
}

func Temperature(cityURL string) interface{} {
	client := &http.Client{}
	var url = fmt.Sprintf("%v", cityURL)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		fmt.Print(err.Error())
	}
	res, err := client.Do(req)
	if err != nil {
		fmt.Print(err.Error())
	}
	defer res.Body.Close()
	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		fmt.Print(err.Error())
	}

	var w map[string]map[string]interface{}
	err1 := json.Unmarshal(body, &w)
	_ = err1
	for k, a := range w {
		if k == "main" {
			for key, value := range a {
				if key == "temp" {
					return value
				}
			}
		}
	}
	return nil
}

func Humidity(cityURL string) interface{} {
	client := &http.Client{}
	var url = fmt.Sprintf("%v", cityURL)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		fmt.Print(err.Error())
	}
	res, err := client.Do(req)
	if err != nil {
		fmt.Print(err.Error())
	}
	defer res.Body.Close()
	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		fmt.Print(err.Error())
	}

	var w map[string]map[string]interface{}
	err1 := json.Unmarshal(body, &w)
	_ = err1
	for k, a := range w {
		if k == "main" {
			for key, value := range a {
				if key == "humidity" {
					return value
				}
			}
		}
	}
	return nil
}

func WindSpeed(cityURL string) interface{} {
	client := &http.Client{}
	var url = fmt.Sprintf("%v", cityURL)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		fmt.Print(err.Error())
	}
	res, err := client.Do(req)
	if err != nil {
		fmt.Print(err.Error())
	}
	defer res.Body.Close()
	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		fmt.Print(err.Error())
	}

	var w map[string]map[string]interface{}
	err1 := json.Unmarshal(body, &w)
	_ = err1
	for k, a := range w {
		if k == "wind" {
			for key, value := range a {
				if key == "speed" {
					return value
				}
			}
		}
	}
	return nil
}

func (o *OpenWeatherAPI) GetTemperature(location string) interface{} {
	switch location {
	case "Москва":
		var url = fmt.Sprintf("https://api.openweathermap.org/data/2.5/weather?q=Moscow&cnt=4&units=metric&lang=ru&appid=7222135c88dedf9e6357a5d2ce0a7d0e") // инициализируйте со своим ключом
		return Temperature(url)
	case "Санкт-Петербург":
		var url = fmt.Sprintf("https://api.openweathermap.org/data/2.5/weather?lat=59.89&lon=30.26&cnt=4&units=metric&lang=ru&appid=7222135c88dedf9e6357a5d2ce0a7d0e") // инициализируйте со своим ключом
		return Temperature(url)
	case "Казань":
		var url = fmt.Sprintf("https://api.openweathermap.org/data/2.5/weather?q=Kazan&cnt=4&units=metric&lang=ru&appid=7222135c88dedf9e6357a5d2ce0a7d0e") // инициализируйте со своим ключом
		return Temperature(url)
	case "Якутск":
		var url = fmt.Sprintf("https://api.openweathermap.org/data/2.5/weather?q=Yakutsk&cnt=4&units=metric&lang=ru&appid=7222135c88dedf9e6357a5d2ce0a7d0e") // инициализируйте со своим ключом
		return Temperature(url)
	}

	return nil
}

func (o *OpenWeatherAPI) GetHumidity(location string) interface{} {
	switch location {
	case "Москва":
		var url = fmt.Sprintf("https://api.openweathermap.org/data/2.5/weather?q=Moscow&cnt=4&units=metric&lang=ru&appid=7222135c88dedf9e6357a5d2ce0a7d0e") // инициализируйте со своим ключом
		return Humidity(url)
	case "Санкт-Петербург":
		var url = fmt.Sprintf("https://api.openweathermap.org/data/2.5/weather?lat=59.89&lon=30.26&cnt=4&units=metric&lang=ru&appid=7222135c88dedf9e6357a5d2ce0a7d0e") // инициализируйте со своим ключом
		return Humidity(url)
	case "Казань":
		var url = fmt.Sprintf("https://api.openweathermap.org/data/2.5/weather?q=Kazan&cnt=4&units=metric&lang=ru&appid=7222135c88dedf9e6357a5d2ce0a7d0e") // инициализируйте со своим ключом
		return Humidity(url)
	case "Якутск":
		var url = fmt.Sprintf("https://api.openweathermap.org/data/2.5/weather?q=Yakutsk&cnt=4&units=metric&lang=ru&appid=7222135c88dedf9e6357a5d2ce0a7d0e") // инициализируйте со своим ключом
		return Humidity(url)
	}
	return nil
}

func (o *OpenWeatherAPI) GetWindSpeed(location string) interface{} {
	switch location {
	case "Москва":
		var url = fmt.Sprintf("https://api.openweathermap.org/data/2.5/weather?q=Moscow&cnt=4&units=metric&lang=ru&appid=7222135c88dedf9e6357a5d2ce0a7d0e") // инициализируйте со своим ключом
		return WindSpeed(url)
	case "Санкт-Петербург":
		var url = fmt.Sprintf("https://api.openweathermap.org/data/2.5/weather?lat=59.89&lon=30.26&cnt=4&units=metric&lang=ru&appid=7222135c88dedf9e6357a5d2ce0a7d0e") // инициализируйте со своим ключом
		return WindSpeed(url)
	case "Казань":
		var url = fmt.Sprintf("https://api.openweathermap.org/data/2.5/weather?q=Kazan&cnt=4&units=metric&lang=ru&appid=7222135c88dedf9e6357a5d2ce0a7d0e") // инициализируйте со своим ключом
		return WindSpeed(url)
	case "Якутск":
		var url = fmt.Sprintf("https://api.openweathermap.org/data/2.5/weather?q=Yakutsk&cnt=4&units=metric&lang=ru&appid=7222135c88dedf9e6357a5d2ce0a7d0e") // инициализируйте со своим ключом
		return WindSpeed(url)
	}
	return nil
}

// WeatherFacade is the facade that provides a simplified interface to the weather API
type WeatherFacade struct {
	weatherAPI WeatherAPI
}

func (w *WeatherFacade) GetWeatherInfo(location string) (interface{}, interface{}, interface{}) {
	temperature := w.weatherAPI.GetTemperature(location)
	humidity := w.weatherAPI.GetHumidity(location)
	windSpeed := w.weatherAPI.GetWindSpeed(location)

	return temperature, humidity, windSpeed
}

func NewWeatherFacade(apiKey string) WeatherFacade {
	return WeatherFacade{
		weatherAPI: &OpenWeatherAPI{apiKey: apiKey},
	}
}

func main() {
	weatherFacade := NewWeatherFacade("api_key")
	cities := []string{"Москва", "Санкт-Петербург", "Казань", "Якутск"}

	for _, city := range cities {
		temperature, humidity, windSpeed := weatherFacade.GetWeatherInfo(city)
		fmt.Printf("Temperature in "+city+": %v\n", temperature)
		fmt.Printf("Humidity in "+city+": %v\n", humidity)
		fmt.Printf("Wind speed in "+city+": %v\n\n", windSpeed)
	}
}
