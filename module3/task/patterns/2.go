package main

import "fmt"

// AirConditioner is the interface that defines the methods for controlling the air conditioner
type AirConditioner interface {
	TurnOn()
	TurnOff()
	SetTemperature(temp int)
}

// RealAirConditioner is the implementation of the air conditioner
type RealAirConditioner struct {
}

func (r *RealAirConditioner) TurnOn() {

}
func (r *RealAirConditioner) TurnOff() {

}
func (r *RealAirConditioner) SetTemperature(temp int) {

}

type AirConditionerAdapter struct {
	airConditioner *RealAirConditioner
}

func (adapter *AirConditionerAdapter) TurnOn() {
	adapter.airConditioner.TurnOn()
}

func (adapter *AirConditionerAdapter) TurnOff() {
	adapter.airConditioner.TurnOff()
}

func (adapter *AirConditionerAdapter) SetTemperature(i int) {
	adapter.airConditioner.SetTemperature(i)
}

type AirConditionerProxy struct {
	adapter       *AirConditionerAdapter
	authenticated bool
}

func NewAirConditionerProxy(authenticated bool) *AirConditionerProxy {
	return &AirConditionerProxy{
		authenticated: authenticated,
	}
}

func (adapter *AirConditionerProxy) TurnOn() {
	if adapter.authenticated == true {
		fmt.Println("Turning on the air conditioner")
	} else {
		fmt.Println("Access denied: authentication required to turn on the air conditioner")
	}
}

func (adapter *AirConditionerProxy) TurnOff() {
	if adapter.authenticated == true {
		fmt.Println("Turning off the air conditioner")
	} else {
		fmt.Println("Access denied: authentication required to turn off the air conditioner")
	}
}

func (adapter *AirConditionerProxy) SetTemperature(i int) {
	if adapter.authenticated == true {
		fmt.Println("Setting air conditioner temperature to", i)
		return
	} else {
		fmt.Println("Access denied: authentication required to set the temperature of the air conditioner")
	}
}

func main() {
	airConditioner := NewAirConditionerProxy(false) // without auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)

	airConditioner = NewAirConditionerProxy(true) // with auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)
}
