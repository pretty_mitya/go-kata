package main

import (
	"testing"
	"time"
)

func BenchmarkDoubleLinkedList_LoadData(b *testing.B) {
	dl := &DoubleLinkedList{}

	for i := 0; i < b.N; i++ {
		err1 := dl.LoadData(path)
		if err1 != nil {
			panic(err1)
		}
	}
}

func BenchmarkDoubleLinkedList_Len(b *testing.B) {
	dl := maintests()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		dl.Len()
	}
}

func BenchmarkDoubleLinkedList_Current(b *testing.B) {
	dl := maintests()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_, _ = dl.Current()
	}
}

func BenchmarkDoubleLinkedList_Prev(b *testing.B) {
	dl := maintests()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_, _ = dl.Prev()
	}
}

func BenchmarkDoubleLinkedList_Next(b *testing.B) {
	dl := maintests()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_, _ = dl.Next()
	}
}

func BenchmarkDoubleLinkedList_Index(b *testing.B) {
	dl := maintests()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_, _ = dl.Index()
	}
}

func BenchmarkDoubleLinkedList_Reverse(b *testing.B) {
	dl := maintests()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_, _ = dl.Reverse()
	}
}

func BenchmarkDoubleLinkedList_Delete(b *testing.B) {
	dl := maintests()
	b.ResetTimer()

	for i := 0; i < 1999; i++ {
		_ = dl.Delete(1)
	}
}

func BenchmarkDoubleLinkedList_DeleteCurrent(b *testing.B) {
	dl := maintests()
	b.ResetTimer()

	for i := 0; i < 1999; i++ {
		_ = dl.DeleteCurrent()
	}
}

func BenchmarkDoubleLinkedList_Pop(b *testing.B) {
	dl := maintests()
	b.ResetTimer()

	for i := 0; i < 1999; i++ {
		_, _ = dl.Pop()
	}
}

func BenchmarkDoubleLinkedList_Shift(b *testing.B) {
	dl := maintests()
	b.ResetTimer()

	for i := 0; i < 1999; i++ {
		_, _ = dl.Shift()
	}
}

func BenchmarkDoubleLinkedList_SearchUUID(b *testing.B) {
	dl := maintests()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_, _ = dl.SearchUUID(dl.head.data.UUID)
	}
}

func BenchmarkDoubleLinkedList_Search(b *testing.B) {
	dl := maintests()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_, _ = dl.SearchUUID(dl.head.data.Message)
	}
}

func BenchmarkDoubleLinkedList_Insert(b *testing.B) {
	dl := maintests()
	example := &Commit{
		Message: "1",
		UUID:    "12",
		Date:    time.Time{}}
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_ = dl.Insert(1, *example)
	}
}
