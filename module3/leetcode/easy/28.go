package main

import "fmt"

func countDigits(num int) int {
	count := 0
	num1 := num
	for num1 > 0 {
		a1 := num1 % 10
		if num%a1 == 0 {
			count++
		}
		num1 /= 10
	}
	return count
	//if num >= 10 && num < 100 {
	//	for {
	//		a1 := num / 10
	//		if num%a1 == 0 {
	//			count++
	//		}
	//		a2 := num % 10
	//		if num%a2 == 0 {
	//			count++
	//		}
	//		break
	//	}
	//} else if num >= 100 && num < 1000 {
	//	for {
	//		a1 := num / 100
	//		if num%a1 == 0 {
	//			count++
	//		}
	//		a2 := num % 100 / 10
	//		if num%a2 == 0 {
	//			count++
	//		}
	//		a3 := num % 10
	//		if num%a3 == 0 {
	//			count++
	//		}
	//		break
	//	}
	//} else if num >= 1000 && num < 10000 {
	//	for {
	//		a1 := num / 1000
	//		if num%a1 == 0 {
	//			count++
	//		}
	//		a2 := num % 1000 / 100
	//		if num%a2 == 0 {
	//			count++
	//		}
	//		a3 := num % 100 / 10
	//		if num%a3 == 0 {
	//			count++
	//		}
	//		a4 := num % 10
	//		if num%a4 == 0 {
	//			count++
	//		}
	//		break
	//	}
	//} else if num >= 1 && num < 10 {
	//	count++
	//}
	//return count
}

func main() {
	fmt.Println(countDigits(121))
}
