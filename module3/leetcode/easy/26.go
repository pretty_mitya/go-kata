// accepted
package main

import "fmt"

func decompressRLElist(nums []int) []int {
	count := 0
	b := []int{}
	for index, i := range nums {
		if index%2 == 0 || index == 0 {
			count = 0
			count = i
		} else {
			for j := 0; j < count; j++ {
				b = append(b, i)
			}
		}
	}
	return b
}

func main() {
	fmt.Println(decompressRLElist([]int{1, 2, 3, 4}))
}
