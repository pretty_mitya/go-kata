// accepted
package main

import (
	"fmt"
	"strings"
)

func interpret(command string) string {
	var command1 string

	command1 = strings.ReplaceAll(command, "(al)", "al")
	command1 = strings.ReplaceAll(command1, "()", "o")

	return command1
}

func main() {
	fmt.Println(interpret("(al)G(al)()()G"))
}
