// accepted
package main

import "fmt"

func numIdenticalPairs(nums []int) int {
	count := 0
	for index := 0; index < len(nums); index++ {
		for index1 := index; index1 < len(nums); index1++ {
			if nums[index] == nums[index1] && index < index1 {
				count++
			}
		}
	}
	return count
}

func main() {
	operations := []int{1, 2, 3, 1, 1, 3}
	fmt.Println(numIdenticalPairs(operations))
}
