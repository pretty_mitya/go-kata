// accepted
package main

import "fmt"

func finalValueAfterOperations(operations []string) int {
	count := 0
	for _, a := range operations {
		if a == "++X" || a == "X++" {
			count++
		} else if a == "--X" || a == "X--" {
			count--
		}
	}
	return count
}

func main() {
	operations := []string{"--X", "X++", "X++"}
	fmt.Println(finalValueAfterOperations(operations))
}
