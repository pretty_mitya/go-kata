package main

import (
	"fmt"
	"strings"
)

func balancedStringSplit(s string) int {
	countR := 0
	countL := 0
	countcmn := 0
	s1 := strings.Split(s, "")
	for _, i := range s1 {
		if i == "R" {
			countR++
		} else if i == "L" {
			countL++
		}
		if countR == countL {
			countcmn++
			countR = 0
			countL = 0
		}
	}
	fmt.Println(countcmn)
	return countcmn
}

func main() {
	fmt.Println(balancedStringSplit("RLRRLLRLRL"))
}
