// accepted
package main

import (
	"fmt"
	"strings"
)

func defangIPaddr(address string) string {
	testresults := strings.ReplaceAll(address, ".", "[.]")
	return testresults
}

func main() {
	fmt.Println(defangIPaddr("2.2.2.1"))
}
