// accepted
package main

import "fmt"

func createTargetArray(nums []int, index []int) []int {
	a := make([]int, len(nums))
	for indexx, i := range index {
		for j := len(a) - 1; i < j; j-- {
			//fmt.Println(a[j], a[j-1])
			a[j] = a[j-1]
			//fmt.Println("->", a)
		}
		a[i] = nums[indexx]
		//fmt.Println("-->", a[i], nums[indexx])
		//fmt.Println("--->", a)
	}
	return a
}

func main() {
	fmt.Println(createTargetArray([]int{0, 1, 2, 3, 4}, []int{0, 1, 2, 2, 1}))
}
