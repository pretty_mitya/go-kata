// accepted
package main

import (
	"fmt"
	"unicode"
)

func numJewelsInStones(jewels string, stones string) int {
	count := 0
	for _, a := range jewels {
		for _, j := range stones {
			if a == j {
				if unicode.IsUpper(a) == true && unicode.IsUpper(j) == true {
					count++
				} else if unicode.IsLower(a) == true && unicode.IsLower(j) == true {
					count++
				}
			}
		}
	}
	return count
}

func main() {
	fmt.Println(numJewelsInStones("zZ", "ZZ"))
}
