// accepted
package main

import "fmt"

func maximumWealth(accounts [][]int) int {
	var sum int
	count := 0
	for _, a := range accounts {
		for _, b := range a {
			sum += b
			fmt.Println(b)
			fmt.Println("-->", sum)
		}
		if sum > count {
			count = sum
			sum = 0
		} else {
			sum = 0
		}
	}
	return count
}

func main() {
	fmt.Println(maximumWealth([][]int{{2, 8, 7}, {7, 1, 3}, {1, 9, 5}}))
}
