// accepted
package main

import "fmt"

func numberOfMatches(n int) int {
	count := 0
	for i := 0; i <= 200; i++ {
		if n%2 == 0 {
			count += n / 2
			n = n / 2
		} else if n == 1 {
			break
		} else {
			count += (n - 1) / 2
			n = ((n-1)/2 + 1)
		}
	}
	return count
}

func main() {
	fmt.Println("total", numberOfMatches(14))
}
