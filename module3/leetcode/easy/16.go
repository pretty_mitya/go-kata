// accepted
package main

import "fmt"

func smallestEvenMultiple(n int) int {
	i := 1
	for i = 1; ; i++ {
		if i%2 == 0 && i%n == 0 {
			return i
		}

	}
}

func main() {
	fmt.Println(smallestEvenMultiple(77))
}
