// accepted
package main

import (
	"fmt"
	"sort"
)

func minimumSum(num int) int {
	numNew := []int{}
	a1 := num / 1000
	a2 := num % 1000 / 100
	a3 := num % 100 / 10
	a4 := num % 10

	numNew = append(numNew, a1, a2, a3, a4)
	sort.Slice(numNew, func(i, j int) bool {
		return numNew[i] < numNew[j]
	})
	count := numNew[0]*10 + numNew[1]*10 + numNew[2] + numNew[3]
	return count
}

func main() {
	fmt.Println(minimumSum(2932))
}
