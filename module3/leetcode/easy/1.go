// accepted
package main

import (
	"fmt"
)

func tribonacci(n int) int {
	var n1, n2, n3 int
	x := 2
	y := 0
	if n == 0 {
		return n
	} else if n == 1 || n == 2 {
		y = 1
		return y
	} else if n == 3 {
		y = 2
		return y
	} else if n > 3 {
		n1 = 0
		n2, n3 = 1, 1
		for x != n {
			n1, n2, n3 = n2, n3, n1+n2+n3
			fmt.Println(n1, n2, n3)
			x++
		}
		return n3
	}
	return n3
}

func main() {
	fmt.Println(tribonacci(4))
}
