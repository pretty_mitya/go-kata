// accepted
package main

import "fmt"

func getConcatenation(nums []int) []int {
	ans := nums
	for _, a := range nums {
		ans = append(ans, a)
	}
	return ans
}

func main() {
	fmt.Println(getConcatenation([]int{1, 2, 3}))
}
