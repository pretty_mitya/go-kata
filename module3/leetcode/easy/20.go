// accepted
package main

import "fmt"

func kidsWithCandies(candies []int, extraCandies int) []bool {
	count := []bool{}
	count1 := 0
	extranum := 0
	for _, num := range candies {
		extranum = num + extraCandies
		for _, j := range candies {
			if j <= extranum {
				count1++
				if count1 == len(candies) {
					count = append(count, true)
					count1 = 0
				}
			} else {
				count1--
				count = append(count, false)
				break
			}
		}
		count1 = 0
	}
	return count
}

func main() {
	fmt.Println(kidsWithCandies([]int{12, 1, 12}, 10))
}
