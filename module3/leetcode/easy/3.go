// accepted
package main

import "fmt"

func convertTemperature(celsius float64) []float64 {
	kelv := celsius + 273.15
	fahr := celsius*1.8 + 32.00
	return []float64{kelv, fahr}
}

func main() {
	fmt.Println(convertTemperature(36.5))
}
