// accepted
package main

import "fmt"

func runningSum(nums []int) []int {
	num := []int{}
	count := 0
	for _, a := range nums {
		count += a
		num = append(num, count)
		fmt.Println(num)
	}
	return num
}

func main() {
	operations := []int{1, 2, 3, 4}
	fmt.Println(runningSum(operations))
}
