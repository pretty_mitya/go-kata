// accepted
package main

import "fmt"

func differenceOfSum(nums []int) int {
	plus1 := 0
	plus2 := 0
	for _, a := range nums {
		plus1 += a
	}
	for _, a := range nums {
		if a >= 10 && a < 100 {
			a1 := a / 10
			plus2 += a1
			a %= 10
			plus2 += a
		} else if a >= 100 && a < 1000 {
			a1 := a / 100
			plus2 += a1
			a2 := a % 100 / 10
			plus2 += a2
			a %= 10
			plus2 += a
		} else if a >= 1000 && a < 10000 {
			a1 := a / 1000
			plus2 += a1
			a2 := a % 1000 / 100
			plus2 += a2
			a3 := a % 100 / 10
			plus2 += a3
			a %= 10
			plus2 += a
		} else if a >= 1 && a < 10 {
			plus2 += a
		}
	}
	dif := plus2 - plus1
	if dif < 0 {
		return -dif
	}
	return dif
}

func main() {
	a := []int{1, 15, 6, 3}
	fmt.Println(differenceOfSum(a))
}
