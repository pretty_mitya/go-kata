// accepted
package main

import "fmt"

func subtractProductAndSum(n int) int {
	plus2 := 0
	mult := 1
	dev := 0
	if n >= 10 && n < 100 {
		a1 := n / 10
		plus2 += a1
		mult *= a1
		n %= 10
		mult *= n
		plus2 += n
		dev = mult - plus2
	} else if n >= 100 && n < 1000 {
		a1 := n / 100
		plus2 += a1
		mult *= a1
		a2 := n % 100 / 10
		plus2 += a2
		mult *= a2
		n %= 10
		mult *= n
		plus2 += n
		dev = mult - plus2
	} else if n >= 1000 && n < 10000 {
		a1 := n / 1000
		plus2 += a1
		mult *= a1
		a2 := n % 1000 / 100
		plus2 += a2
		mult *= a2
		a3 := n % 100 / 10
		plus2 += a3
		mult *= a3
		n %= 10
		mult *= n
		plus2 += n
		dev = mult - plus2
	} else if n >= 10000 && n < 100000 {
		a1 := n / 10000
		plus2 += a1
		mult *= a1
		a2 := n % 10000 / 1000
		plus2 += a2
		mult *= a2
		a3 := n % 1000 / 100
		plus2 += a3
		mult *= a3
		a4 := n % 100 / 10
		plus2 += a4
		mult *= a4
		n %= 10
		mult *= n
		plus2 += n
		dev = mult - plus2
	} else if n >= 1 && n < 10 {
		mult *= n
		plus2 += n
		dev = mult - plus2
	}
	return dev
}

func main() {
	fmt.Println(subtractProductAndSum(35252))
}
