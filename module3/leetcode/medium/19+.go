// accepted
package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

var counter int

func averageOfSubtree(root *TreeNode) int {
	//c := 0
	//a := func(count *int, root *TreeNode) (int, int,int) {
	//	if root == nil {
	//		return nil
	//	}
	//}
	counter = 0
	helper(root)
	return counter
}

func helper(root *TreeNode) (c, summa int) {
	if root == nil {
		return 0, 0
	}
	lc, lsumma := helper(root.Left)
	rc, rsumma := helper(root.Right)
	c = lc + rc + 1
	summa = root.Val + lsumma + rsumma

	//count for av
	if summa/c == root.Val {
		counter++
	}
	return c, summa
}
