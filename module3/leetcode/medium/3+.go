// accepted
package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func (l *ListNode) Insert(d int) {
	list := &ListNode{
		Val:  d,
		Next: nil,
	}
	if l.Next == nil {
		l.Next = list
	} else {
		p := l.Next
		for p.Next != nil {
			p = p.Next
		}
		p.Next = list
	}
}

func Show(l *ListNode) {
	p := l.Next
	for p != nil {
		fmt.Printf("-> %v ", p.Val)
		p = p.Next
	}
}

func mergeNodes(head *ListNode) *ListNode {
	list := &ListNode{}
	var count int
	exam := list
	if head == nil {
		return nil
	}
	for head != nil {
		if head.Val == 0 && count > 0 {
			exam.Next = &ListNode{count, nil}
			exam = exam.Next
			count = 0
		}
		count += head.Val
		head = head.Next
	}
	return list.Next
}

func main() {
	l := ListNode{}
	l.Insert(0)
	l.Insert(3)
	l.Insert(1)
	l.Insert(0)
	l.Insert(4)
	l.Insert(5)
	l.Insert(2)
	l.Insert(0)

	Show(mergeNodes(&l))
}
