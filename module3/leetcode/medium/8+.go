// accepted
package main

import "fmt"

func findSmallestSetOfVertices(n int, edges [][]int) []int {
	//a := make(map[int]int)
	a := make([]int, n)
	answer := []int{}

	for _, v := range edges {
		fmt.Println("V = ", v)
		a[v[1]]++
		fmt.Println("a[v[1]] = ", a[v[1]])
	}

	for i := range a {
		if i == n {
			break
		} else if a[i] == 0 {
			answer = append(answer, i)
		}
	}
	return answer

}

func main() {
	fmt.Println(findSmallestSetOfVertices(6, [][]int{{0, 1}, {0, 2}, {2, 5}, {3, 4}, {4, 2}}))
}
