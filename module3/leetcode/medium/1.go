package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func deepestLeavesSum(root *TreeNode) int {
	if root == nil {
		return 0
	}
	var a, b, c int
	if root.Left == nil && root.Right == nil {
		if a > b {
			b = a
			c = root.Val
		} else if a < b {
			return 0
		} else {
			c += root.Val
		}
	}

}
