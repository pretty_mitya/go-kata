// accepted
package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func mergeInBetween(list1 *ListNode, a int, b int, list2 *ListNode) *ListNode {
	list := &ListNode{}
	list11 := list1
	end := b - a + 2
	list22 := list2

	count := 0
	for list22 != nil {
		if list22.Next == nil {
			list = list22
		}
		list22 = list22.Next
	}
	//for i := 0; i <= b; i++ {
	//	switch {
	//	case i < a:
	//		list.Insert(i)
	//	case i >= a:
	//		break
	//	}
	//}
	//list = joinLists(list, list2)
	//list = joinLists(list, list1)
	//
	//return list2

	for list11 != nil {
		if count == a-1 {
			listif := list11
			for i := 0; i < end; i++ {
				listif = listif.Next
			}
			list11.Next = list2
			list.Next = listif
			return list1
		}
		count++
		list11 = list11.Next
	}
	return &ListNode{}
}

func (d *ListNode) Delete(n int) error {
	if n == 0 {
		fmt.Println("--->n must me positive and not nil")
		return nil
	}
	i := 0
	for d != nil {
		i++
		if i == n-1 {
			nextnode := d.Next
			d.Next = nextnode.Next
			nextnode.Next = nil
			break
		}
	}
	d.Next = d.Next.Next
	return nil
}

func joinLists(li1, li2 *ListNode) *ListNode {
	if li1 == nil {
		return li2
	} else if li2 == nil {
		return li1
	}

	current := li1
	for current.Next != nil {
		current = current.Next
	}
	current.Next = li2
	return li1
}
func (l *ListNode) Insert(d int) {
	list := &ListNode{
		Val:  d,
		Next: nil,
	}
	if l.Next == nil {
		l.Next = list
	} else {
		p := l.Next
		for p.Next != nil {
			p = p.Next
		}
		p.Next = list
	}
}

func Show(l *ListNode) {
	p := l.Next
	for p != nil {
		fmt.Printf("-> %v ", p.Val)
		p = p.Next
	}
}

func main() {
	l := ListNode{}
	l.Insert(0)
	l.Insert(1)
	l.Insert(2)
	l.Insert(3)
	l.Insert(4)
	l.Insert(5)
	l2 := ListNode{}
	l2.Insert(1000000)
	l2.Insert(1000001)
	l2.Insert(1000002)
	Show(mergeInBetween(&l, 3, 4, &l2))
}
