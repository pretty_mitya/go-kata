// accepted
package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

//func (l *TreeNode) Insert(d int) {
//	list := &ListNode{
//		Val:  d,
//		Next: nil,
//	}
//	if l.r == nil {
//		l.Next = list
//	} else {
//		p := l.Next
//		for p.Next != nil {
//			p = p.Next
//		}
//		p.Next = list
//	}
//}
//
//func Show(l *ListNode) {
//	p := l.Next
//	for p != nil {
//		fmt.Printf("-> %v ", p.Val)
//		p = p.Next
//	}
//}

func removeLeafNodes(root *TreeNode, target int) *TreeNode {
	if root == nil {
		return root
	}
	root.Left = removeLeafNodes(root.Left, target)
	root.Right = removeLeafNodes(root.Right, target)
	//return root
	//switch {
	//case root.Left == nil:
	//	continue
	//}
	if root.Left == nil && root.Right == nil && root.Val == target {
		return nil
	} else {
		return root
	}
	return root
}
