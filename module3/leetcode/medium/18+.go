// accepted
package main

import "fmt"

func groupThePeople(groupSizes []int) [][]int {
	var answer [][]int
	mapforgroup := make(map[int][]int)
	for i, a := range groupSizes {
		mapforgroup[a] = append(mapforgroup[a], i)

		if len(mapforgroup[a]) == a {
			answer = append(answer, mapforgroup[a])
			mapforgroup[a] = nil
		}
	}
	for i, a := range mapforgroup {
		for len(a) != 0 {
			digit := len(a) - i
			answer = append(answer, a[digit:])
		}
	}
	return answer
}

func main() {
	fmt.Println(groupThePeople([]int{3, 3, 3, 3, 3, 1, 3}))
}
