// accepted
package main

import (
	"fmt"
	"math"
)

func countPoints(points [][]int, queries [][]int) []int {
	answer := []int{}
	for _, a := range queries {
		count := 0
		for _, b := range points {
			fl1 := float64(a[0] - b[0])
			fl2 := float64(a[1] - b[1])
			if math.Pow(fl1, 2)+math.Pow(fl2, 2) <= math.Pow(float64(a[2]), 2) {
				count++
			}
		}
		answer = append(answer, count)
	}
	return answer
}

func main() {
	fmt.Println(countPoints([][]int{{1, 3}, {3, 3}, {5, 3}, {2, 2}}, [][]int{{2, 3, 1}, {4, 3, 1}, {1, 1, 2}}))
}
