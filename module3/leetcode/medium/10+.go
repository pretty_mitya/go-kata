// accepted
package main

import "fmt"

func numTilePossibilities(tiles string) int {
	//инглиш букв
	common := make([]int, 26)
	for _, a := range tiles {
		digit := a - 'A'
		common[digit]++
	}
	return auto(common)
}

func auto(arr []int) int {
	var sum int
	for i := 0; i < len(arr); i++ {
		if arr[i] == 0 {
			continue
		}
		sum++
		arr[i]--
		sum += auto(arr)
		arr[i]++
	}
	return sum
}

func main() {
	fmt.Println(numTilePossibilities("AAABBC"))
}
