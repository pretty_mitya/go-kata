// accepted
package main

import "fmt"

func processQueries(queries []int, m int) []int {
	nums := make([]int, m)

	for i := 0; i < m; i++ {
		nums[i] = i + 1
	}

	answer := make([]int, len(queries))

	for i, a := range queries {
		j := 0
		for nums[j] != a {
			//fmt.Println(nums[j])
			j++
		}
		answer[i] = j
		for ; j > 0; j-- {

			nums[j] = nums[j-1]
			fmt.Println(nums[j])
		}
		nums[0] = a
	}
	return answer
}

func main() {
	fmt.Println(processQueries([]int{3, 1, 2, 1}, 5))
}
