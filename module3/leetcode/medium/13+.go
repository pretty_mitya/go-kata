// accepted
package main

import "fmt"

func maxSum(grid [][]int) int {
	maximum := 0
	for i := 1; i < len(grid)-1; i++ {
		for j := 1; j < len(grid[0])-1; j++ {
			sum := Summa(i, j, grid)
			if sum > maximum {
				maximum = sum
			}
		}
	}
	return maximum
}

func Summa(i, j int, grid [][]int) int {
	a := grid[i][j]
	b := j - 1
	end := j + 1
	for ; b <= end; b++ {
		a += grid[i-1][b]
		a += grid[i+1][b]
	}
	return a
}

func main() {
	fmt.Println(maxSum([][]int{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}))
}
