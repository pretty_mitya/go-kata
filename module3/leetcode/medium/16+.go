// accepted
package main

var slices [][]int

type SubrectangleQueries struct {
	rectangle [][]int
}

func Constructor(rectangle [][]int) SubrectangleQueries {
	a := SubrectangleQueries{rectangle: rectangle}
	return a
}

func (this *SubrectangleQueries) UpdateSubrectangle(row1 int, col1 int, row2 int, col2 int, newValue int) {
	newone := []int{row1, col1, row2, col2, newValue}
	slices = append(slices, newone)
}

func (this *SubrectangleQueries) GetValue(row int, col int) int {
	i := len(slices) - 1
	for ; i >= 0; i-- {
		//v := slices[i]
		if slices[i][0] <= row && slices[i][1] <= col && slices[i][2] >= row && slices[i][3] >= col {
			return slices[i][4]
		}
	}
	return this.rectangle[row][col]
}
