// accepted
package main

import (
	"fmt"
	"sort"
)

func sortTheStudents(score [][]int, k int) [][]int {
	sort.SliceStable(score, func(p, q int) bool {
		return score[p][k] > score[q][k]
	})
	return score
}

func main() {
	fmt.Println(sortTheStudents([][]int{{3, 4}, {5, 6}}, 0))
}
