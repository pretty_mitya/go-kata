// accepted
package main

import (
	"fmt"
)

func minPartitions(n string) int {
	digit := 0
	for i, _ := range n {
		fmt.Println(n[i])
		if digit < int(n[i]-'0') {
			digit = int(n[i] - '0')
		}
	}
	return digit
}
func main() {
	fmt.Println(minPartitions("32"))
}
