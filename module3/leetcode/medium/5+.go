// accepted
package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func (l *ListNode) Insert(d int) {
	list := &ListNode{
		Val:  d,
		Next: nil,
	}
	if l.Next == nil {
		l.Next = list
	} else {
		p := l.Next
		for p.Next != nil {
			p = p.Next
		}
		p.Next = list
	}
}

func Show(l *ListNode) {
	p := l.Next
	for p != nil {
		fmt.Printf("-> %v ", p.Val)
		p = p.Next
	}
}

func pairSum(head *ListNode) int {
	//list := &ListNode{}
	var slice []int
	for head != nil {
		slice = append(slice, head.Val)
		head = head.Next
	}
	var res int
	//i := -1
	//twin := 0
	//sum := 0
	//sum1 := 0

	for i := 0; i <= (len(slice)/2)-1; i++ {

		ii := len(slice) - 1 - i
		v := slice[i] + slice[ii]
		if v > res {
			res = v
		}
	}
	//
	//for head.Next != nil {
	//	i++
	//	head = head.Next
	//	if (n/2)-1 >= i {
	//		list.Insert(head.Val)
	//		twin = n - 1 - i
	//		fmt.Println("i", twin, i)
	//	}
	//	fmt.Println("i", i)
	//	if i == twin {
	//		fmt.Println(twin, i)
	//		list.Insert(head.Val)
	//		Show(list)
	//		sum = head.Val + head.Next.Val
	//		//fmt.Println("sum = ", sum)
	//		if sum1 < sum {
	//			sum1 = sum
	//		}
	//	}
	//}
	return res
}

func main() {
	l := ListNode{}
	l.Insert(7)
	fmt.Println(pairSum(&l))

}
