// accepted
package main

import (
	"fmt"
	"sort"
)

func checkArithmeticSubarrays(nums []int, l []int, r []int) []bool {
	var answer []bool
	//s := []int{}
	a := func(slice1 []int) bool {
		for i := 1; i < len(slice1)-1; i++ {
			//if i == len(nums)-1 {
			//	break
			//}
			if slice1[i]-slice1[i+1] != slice1[i-1]-slice1[i] {
				return false
			}
		}
		return true
	}
	for i := range l {
		var slice []int
		slice = append(slice, nums[l[i]:r[i]+1]...)

		//sort.Sort(sort.IntSlice(slice))
		sort.Slice(slice, func(i, j int) bool { return slice[j] > slice[i] })
		fmt.Println(slice)
		answer = append(answer, a(slice))
	}
	return answer
}

func main() {
	fmt.Println(checkArithmeticSubarrays([]int{4, 6, 5, 9, 3, 7}, []int{0, 0, 2}, []int{2, 3, 5}))
}
