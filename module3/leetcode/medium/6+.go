// accepted
package main

import "fmt"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func constructMaximumBinaryTree(nums []int) *TreeNode {
	if len(nums) == 0 {
		return nil
	}
	var maximum int
	var index int
	for i, a := range nums {
		switch {
		case i == 0:
			maximum = a
			index = i
		case a > maximum:
			maximum = a
			index = i
		}
	}
	fmt.Println(maximum)
	fmt.Println(index)
	return &TreeNode{maximum, constructMaximumBinaryTree(nums[:index]), constructMaximumBinaryTree(nums[index+1:])}
}

func main() {
	fmt.Println(constructMaximumBinaryTree([]int{3, 2, 1, 6, 0, 5}))
}
