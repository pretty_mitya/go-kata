package main

func BubbleSort(data []int) []int {
	swapped := true
	i := 1
	for swapped {
		swapped = false
		for j := 0; j < len(data)-i; j++ {
			if data[j] > data[j+1] {
				data[j], data[j+1] = data[j+1], data[j]
				swapped = true
			}
		}
		i++

	}
	return data
}
