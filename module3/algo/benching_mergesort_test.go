package main

import (
	"testing"
)

func BenchmarkMergeSort(b *testing.B) {
	data := generateData(b.N, 1000, 10000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		mergeSort(data[i])
	}

}
