package main

import (
	"testing"
)

func BenchmarkQuickSort(b *testing.B) {
	dataSet := generateData(b.N, 1000, 5000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		QuickSort(dataSet[i])
	}

}
