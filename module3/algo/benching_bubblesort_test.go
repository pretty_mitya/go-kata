package main

import (
	"testing"
)

func BenchmarkBubbleSort(b *testing.B) {
	data := generateData(b.N, 1000, 5000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		BubbleSort(data[i])
	}

}
