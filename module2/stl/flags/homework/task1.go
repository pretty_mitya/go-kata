package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

type Config struct {
	AppName    string `json:"appName"`
	Production bool   `json:"production"`
}

func main() {
	conf := Config{}
	var output string

	flag.StringVar(&output, "conf", "", "Writes output to the file specified")
	flag.Parse()

	jsonFile, err := os.Open(output)
	if err != nil {
		fmt.Println(err)
	}
	defer jsonFile.Close()

	data, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		fmt.Println(err)
	}

	jsonerr := json.Unmarshal(data, &conf)
	if jsonerr != nil {
		log.Fatal(jsonerr)
	}
	fmt.Println("Flag:", output)
	fmt.Println("Production:", conf.Production)
	fmt.Println("AppName:", conf.AppName)

}
