package main

import (
	"fmt"
	"reflect"
	"time"
)

var reflectedStructs map[string][]Field

type Field struct {
	Name string
	Tags map[string]string
}

type UserDTO struct {
	ID            int       `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	Name          string    `json:"name" db:"name" db_type:"varchar(55)" db_default:"default null" db_ops:"create,update"`
	Phone         string    `json:"phone" db:"phone" db_type:"varchar(34)" db_default:"default null" db_index:"index,unique" db_ops:"create,update"`
	Email         string    `json:"email" db:"email" db_type:"varchar(89)" db_default:"default null" db_index:"index,unique" db_ops:"create,update"`
	Password      string    `json:"password" db:"password" db_type:"varchar(144)" db_default:"default null" db_ops:"create,update"`
	Status        int       `json:"status" db:"status" db_type:"int" db_default:"default 0" db_ops:"create,update"`
	Role          int       `json:"role" db:"role" db_type:"int" db_default:"not null" db_ops:"create,update"`
	Verified      bool      `json:"verified" db:"verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	EmailVerified bool      `json:"email_verified" db:"email_verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	PhoneVerified bool      `json:"phone_verified" db:"phone_verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	CreatedAt     time.Time `json:"created_at" db:"created_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	UpdatedAt     time.Time `json:"updated_at" db:"updated_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	DeletedAt     time.Time `json:"deleted_at" db:"deleted_at" db_type:"timestamp" db_default:"default null" db_index:"index"`
}

type EmailVerifyDTO struct {
	ID        int       `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	Email     string    `json:"email" db:"email" db_type:"varchar(89)" db_default:"not null" db_index:"index,unique" db_ops:"create,update"`
	UserID    int       `json:"user_id,omitempty" db:"user_id" db_ops:"create" db_type:"int" db_default:"not null" db_index:"index"`
	Hash      string    `json:"hash,omitempty" db:"hash" db_ops:"create" db_type:"char(36)" db_default:"not null" db_index:"index"`
	Verified  bool      `json:"verified" db:"verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	CreatedAt time.Time `json:"created_at" db:"created_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
}

func main() {
	structs := []interface{}{
		UserDTO{},
		EmailVerifyDTO{},
	}
	var structuraslice []Field
	reflectedStructs = make(map[string][]Field, len(structs))

	for a := range structs {
		typestruct := reflect.TypeOf(structs[a])
		firststructname := typestruct.Name()
		for i := 0; i < typestruct.NumField(); i++ {
			secondstructname := typestruct.Field(i).Name
			commonMap := make(map[string]string)
			commonMap["json"] = getStructTag(typestruct.Field(i), "json")
			commonMap["db"] = getStructTag(typestruct.Field(i), "db")
			commonMap["db_type"] = getStructTag(typestruct.Field(i), "db_type")
			commonMap["db_default"] = getStructTag(typestruct.Field(i), "db_default")
			commonMap["db_ops"] = getStructTag(typestruct.Field(i), "db_ops")
			commonMap["db_index"] = getStructTag(typestruct.Field(i), "db_index")
			commonMap["json"] = getStructTag(typestruct.Field(i), "json")
			commonMap["db"] = getStructTag(typestruct.Field(i), "db")
			commonMap["db_type"] = getStructTag(typestruct.Field(i), "db_type")
			commonMap["db_default"] = getStructTag(typestruct.Field(i), "db_default")
			commonMap["db_ops"] = getStructTag(typestruct.Field(i), "db_ops")
			commonMap["db_index"] = getStructTag(typestruct.Field(i), "db_index")

			//setValue(&Field{}, "Tags", commonMap)
			//name := getValue(&Field{}, "Name")
			structuraslice = append(structuraslice, Field{secondstructname, commonMap})
		}
		reflectedStructs[firststructname] = structuraslice
	}
	fmt.Println("HI, THIS STRUCTURE with MAP tags:", reflectedStructs)
}

// заполни данными структур согласно заданиям
//reflectedStructs = make(map[string][]Field, len(structs))
//fmt.Println(reflectedStructs)

func getStructTag(f reflect.StructField, tagName string) string {
	return string(f.Tag.Get(tagName))
}

func setValue(structure interface{}, key string, value interface{}) {
	if reflect.TypeOf(structure).Elem().Kind() == reflect.Struct {
		elem := reflect.ValueOf(structure).Elem()
		field := elem.FieldByName(key)
		if field.CanSet() {
			field.Set(reflect.ValueOf(value))
		} else {
			fmt.Println("Unfortunately, struct is incorrect")
		}
	} else {
		fmt.Println("YOU HAVE MISTAKE")
	}
}

func getValue(structure interface{}, key string) interface{} {
	var result interface{}
	if reflect.TypeOf(structure).Elem().Kind() == reflect.Struct {
		elem := reflect.ValueOf(structure).Elem()
		field := elem.FieldByName(key)
		if field.IsValid() {
			result = field.Interface()
		}
	}
	return result
}
