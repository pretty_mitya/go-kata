package main

import (
	"fmt"
	"io"
	"os"

	"github.com/essentialkaos/translit"

	_ "github.com/essentialkaos/translit"
)

func main() {
	// create final file
	file2, err := os.Create("example.processed.txt")
	if err != nil {
		panic(err)
	}
	defer file2.Close()

	// create first file
	text := "я дима и я димон"
	file1, err := os.Create("example.txt")
	if err != nil {
		panic(err)
	}
	defer file1.Close()
	file1.WriteString(text)
	fmt.Println("first step")

	// open first file and translit
	file1, err = os.Open("example.txt")
	if err != nil {
		panic(err)
	}

	data := make([]byte, 64)
	for {
		n, err := file1.Read(data)
		if err == io.EOF {
			break
		}
		firsttext := string(data[:n])
		fmt.Println(firsttext)
		finaltext := translit.EncodeToALALC(firsttext)

		// write to final file
		defer file1.Close()
		file2.WriteString(finaltext)
	}
}
