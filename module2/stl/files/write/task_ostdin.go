package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	file, err := os.Create("names.txt")
	if err != nil {
		panic(err)
	}

	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Enter your name: ")

	name, _ := reader.ReadString('\n')

	fmt.Printf("Hello %s\n", name)

	defer file.Close()
	file.WriteString(name)
}
