// You can edit this code!
// Click here and start typing.
package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
)

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}
	// здесь расположите буфер
	var writer bytes.Buffer

	// запишите данные в буфер
	for _, p := range data {
		n, err := writer.Write([]byte(p))
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		if n != len(p) {
			fmt.Println("failed to write data")
			os.Exit(1)
		}
		a, _ := writer.Write([]byte("\n"))
		_ = a
	}

	// создайте файл
	file, err := os.Create("example.txt")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	// запишите данные в файл
	file.WriteString(writer.String())

	// прочтите данные в новый буфе
	file, err = os.Open("example.txt")
	if err != nil {
		panic(err)
	}
	opening := make([]byte, 64) // меньше, чем длина источника строки
	for {
		n, err := file.Read(opening)
		if err == io.EOF {
			break
		}
		text := string(opening[:n])
		fmt.Println(text)
	}

	fmt.Println(writer.String()) // выведите данные из буфера buffer.String()
	// у вас все получится!

}
