package main

import (
	"fmt"
	"strconv"
	"strings"
)

type Cache struct {
	data map[string]*User
}

func NewCache() *Cache {
	return &Cache{data: make(map[string]*User, 100)}
}

func (c *Cache) Set(key string, u *User) {
	c.data[key] = u
}

func (c *Cache) Get(key string) *User {
	return c.data[key]
}

type User struct {
	ID       int
	Nickname string
	Email    string
}

func main() {
	var users []*User
	emails := []string{"robpike@gmail.com", "davecheney@gmail.com", "bradfitzpatrick@email.ru", "eliben@gmail.com", "quasilyte@mail.ru"}
	for i, nick := range emails {
		nickname := strings.Split(nick, "@")
		users = append(users, &User{ID: i + 1, Nickname: nickname[0], Email: emails[i]})
		fmt.Println(i, users)
	}
	cache := NewCache()
	for i := range users {
		_ = i
		stringID := strconv.Itoa(users[i].ID)
		//cache.Set(users[i].Nickname, users[i])
		structura := []string{users[i].Nickname, stringID}
		fullbodyuser := strings.Join(structura, ":")
		cache.Set(fullbodyuser, users[i])
		fmt.Println(fullbodyuser)
		// Положить пользователей в кэш с ключом Nickname:userid
		// ...
	}
	keys := []string{"robpike:1", "davecheney:2", "bradfitzpatrick:3", "eliben:4", "quasilyte:5"}
	for i := range keys {
		_ = i
		keysjoin := strings.Join(keys, ", ")
		cache.Get(keysjoin)
	}
}
