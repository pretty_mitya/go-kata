package main

import (
	"fmt"
	"io"
	"os"
)

type Person struct {
	Name  string
	Age   int
	money float64
}

const name, age, money = "Vlad", 25, 10.00000025

func main() {
	generateSelfStory(name, age, money)
}

func generateSelfStory(name string, age int, money float64) string {
	a := fmt.Sprintf("Hello! My name is %s. I'm %d y.o. And I also have $ %v in my wallet right now.", name, age, money)
	io.WriteString(os.Stdout, a)
	return a
}
