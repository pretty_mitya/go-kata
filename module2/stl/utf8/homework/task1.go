package main

import (
	"fmt"
	"strings"
	"unicode/utf8"

	"github.com/essentialkaos/translit"

	_ "github.com/essentialkaos/translit"
)

var data = ` Сообщения с телеграм канала Go-go!
я, извините, решил, что вы хоть что-то про регулярки знаете 🙂
С Почтением, с уважением 🙂.

Сообщения с телеграм канала Go-get job:
#Вакансия #Vacancy #Удаленно #Remote #Golang #Backend  
👨‍💻  Должность: Golang Tech Lead
🏢  Компания: PointPay
💵  Вилка: от 9000 до 15000 $ до вычета налогов
🌏  Локация: Великобритания
💼  Формат работы: удаленный
💼  Занятость: полная занятость 

Наша компания развивает 15 продуктов в сфере блокчейн и криптовалюты, функционирующих в рамках единой экосистемы. Мы - одни из лучших в своей нише, и за без малого три года мы создали криптовалютную платформу, которая предоставляет комплексные решения и позволяет пользователям проводить практически любые финансовые операции.
Наш проект победил в номинации "лучший блокчейн-стартап 2019 года" на одной из самых крупных конференций по блокчейн, криптовалютам и майнингу – Blockchain Life в Москве.

У нас очень амбициозные планы по развитию наших продуктов, и на данный момент мы находимся в поиске Golang Technical Lead, чтобы вместе создать революционный и самый технологичный продукт в мире.


  
Мы ожидаем от тебя:  
  
✅ Опыт управления финансовыми IT продуктами;
✅ Опыт постановки задач технической команде и приема результатов;
✅ Понимание и использование современных подходов к разработке (Agile);
✅ Опыт разработки не менее 5 лет;
✅ Отличное владение Go, будет плюсом опыт работы на PHP;
✅ Опыт работы с REST / GRPС (protobuf);
✅ Опыт работы с Laravel;
✅ Отличные навыки SQL (мы используем PostgreSQL);
✅ Опыт работы с Redis или аналогичной системой кеширования;
✅ Опыт работы с Kubernetes, с брокерами сообщений (RabbitMQ, Kafka и др.);
✅ Базовые знания Amazon Web Services.

Будем плюсом:

✅ Опыт разработки криптовалютных кошельков / криптовалютных бирж / криптовалютных торговых роботов / криптовалютных платежных систем или опыт работы в FinTech или банковских продуктах.

Чем предстоит заниматься:  
  
📌 Управлением и взаимодействием с командой;
📌 Постановкой задач команде разработчиков;
📌 Разработкой решений для запуска и сопровождения разрабатываемого компанией ПО;
📌 Выстраиванием эффективного процесса разработки;
📌 Достигать результатов, поддерживать и развивать текущие проекты, развивать новые проекты и продукты, искать и предлагать нестандартные решения задач.

Мы гарантируем будущему коллеге:  
  
🔥 Высокую заработную плату до 15 000$ (в валюте);
🔥 Индексацию заработной платы;
🔥 Полностью удаленный формат работы и максимально гибкий график - работайте из любой точки мира в удобное вам время;
🔥 Оформление по бессрочному международному трудовому договору с UK;
🔥 Отсутствие бюрократии и кучи бессмысленных звонков;
🔥 Корпоративную технику за счет компании;
🔥 Сотрудничество с командой высококвалифицированных специалистов, настоящих профессионалов своего дела;
🔥 Работу над масштабным, интересным и перспективным проектом.


Если в этом описании вы узнали  себя - пишите. Обсудим проект, задачи и все детали :)  
  
Павел,  
✍️TG: @pavel_hr  
📫 papolushkin.wanted@gmail.com`

func main() {
	// first task
	fmt.Println(utf8.RuneCountInString(data)) //(len([]rune(data)))

	// second task
	s := strings.Split(data, "")
	//s := strings.Fields(data)
	for i := 0; i < len(s); i++ {
		_, size := utf8.DecodeLastRuneInString(s[i])
		if size >= 3 {
			//var emojiRx = regexp.MustCompile(string(a))
			//var s = emojiRx.ReplaceAllString(string(a), `======)))))`)
			//var s = strings.ReplaceAll(r, string(a), "==========)")
			s[i] = strings.Replace(s[i], s[i], "=)", -1)
			//fmt.Println(strings.ReplaceAll(r, string(`[#*0-9]\x{FE0F}?\x{20E3}|©\x{FE0F}?|(?i)[^А-ЯЁA-Z]...`), "=)"))
		}
	}
	finaltext := strings.Join(s, "")
	fmt.Println(finaltext)

	// third task
	data2 := translit.EncodeToALALC(data)
	fmt.Println(data2)
	fmt.Println(utf8.RuneCountInString(data2))

	// forth task
	//fmt.Println(float64(utf8.RuneCountInString(data2)) / float64(utf8.RuneCountInString(data)))
	dat, dat2 := []byte(data), []byte(data2)
	compressionofbytes := float64(len(dat2)) / float64(len(dat))
	fmt.Printf("%.f %.f %.2f", float64(len(dat2)), float64(len(dat)), compressionofbytes)
}

// there are more than 1203i21948312 tries of task with emojis...
//var emojiRx = regexp.MustCompile(`[\x{1F600}-\x{1F6FF}|[\x{2600}-\x{26FF}]`)
//data = emojiRx.ReplaceAllString(data, `=)`)
//--------------------
//re := regexp.MustCompile(`[#*0-9]\x{FE0F}?\x{20E3}|©\x{FE0F}?|(?i)[^А-ЯЁA-Z]...`)
//replaced := re.ReplaceAllStringFunc(data, func(match string) string {
//	r, _ := utf8.DecodeRuneInString(match)
//	return fmt.Sprintf("=)", r)
//})
////
////fmt.Println(replaced) //=> "i like you hahahah &#128512; hello."
////--------------------
//for len(data) > 0 {
//	r, size := utf8.DecodeLastRuneInString(data)
//	if size >= 4 {
//		//s := fmt.Sprintf("%c", r)
//		fmt.Printf(strings.ReplaceAll(data, string(r), "=)"))
//	}
//	//fmt.Printf("%c %v\n", r, size)
//
//	data = data[:len(data)-size]
//}
//fmt.Printf("%T", data)
//---------------------------
//res := ""
//runes := []rune(data)
//
//for i := 0; i < len(runes); i++ {
//	r := runes[i]
//	if r < 128 {
//		res += string(r)
//	} else {
//		res += "&#" + strconv.FormatInt(int64(r), 10) + ";"
//	}
//}
//
//log.Printf("result html string: %v", res)
//---------------------------
//fmt.Println(s)
