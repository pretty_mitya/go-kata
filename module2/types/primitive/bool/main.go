package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 112358132134
	fmt.Println("size is:", unsafe.Sizeof(n), "bytes")
	typeUint()
	typeint()
	typeFloat()
	typeByte()
	typeBool()
}

func typeBool() {
	fmt.Println("=== START type bool ===")
	var b bool
	fmt.Println("Размер в байтах:", unsafe.Sizeof(b))
	var u uint8 = 1
	fmt.Println(b)
	b = *(*bool)(unsafe.Pointer(&u))
	fmt.Println("=== END type bool ===")
}

func typeByte() {
	fmt.Println("=== START type byte ===")
	var b byte = 124
	fmt.Println("Размер в байтах:", unsafe.Sizeof(b))
	fmt.Println("=== END type byte ===")
}

func typeFloat() {
	fmt.Println("=== START type float ===")

	var uintNumber uint32 = 1 << 29
	uintNumber += 1 << 28
	uintNumber += 1 << 27
	uintNumber += 1 << 26
	uintNumber += 1 << 25

	uintNumber += 1 << 21
	uintNumber += 1 << 31
	var floatNumber float32
	floatNumber = *(*float32)(unsafe.Pointer(&uintNumber))
	fmt.Println(floatNumber)

	a, b := 2.3329, 3.1234
	c := a + b
	fmt.Println("пример ошибки1:", c)

	a = 9.99999
	b2 := float64(a)
	fmt.Println("пример ошибки2:", b2)

	a = 999998455
	b3 := float32(a)
	fmt.Printf("пример ошибки 3:", "%f\n", b3)

	a4 := 5.2
	b4 := 4.1
	fmt.Println(a4 + b4)
	fmt.Println((a4 + b4) == 9.3)

	c4 := 5.2
	d4 := 2.1
	fmt.Println(c4 + d4)
	fmt.Println((c4 + d4) == 7.3)
	fmt.Println("=== END type float ===")
}

func typeint() {
	fmt.Println("=== START type int ===")
	var uintNumber uint8 = 1 << 7
	var min = int8(uintNumber)
	uintNumber--
	var max = int8(uintNumber)
	fmt.Println(min, max)
	fmt.Println("int 8 min value:", min, "int 8 max value:", max, "size is:", unsafe.Sizeof(min), "bytes")

	var uintNumber16 uint16 = 1 << 15
	var min16 = int16(uintNumber16)
	uintNumber16--
	var max16 = int16(uintNumber16)
	fmt.Println(min16, max16)
	fmt.Println("int 16 min value:", min16, "int 16 max value:", max16, "size is:", unsafe.Sizeof(min16), "bytes")

	var uintNumber32 uint32 = 1 << 31
	var min32 = int32(uintNumber32)
	uintNumber32--
	var max32 = int32(uintNumber32)
	fmt.Println(min32, max32)
	fmt.Println("int 32 min value:", min32, "int 32 max value:", max32, "size is:", unsafe.Sizeof(min32), "bytes")

	var uintNumber64 uint64 = 1 << 63
	var min64 = int64(uintNumber64)
	uintNumber64--
	var max64 = int64(uintNumber64)
	fmt.Println(min64, max64)
	fmt.Println("int 64 min value:", min64, "int 64 max value:", max64, "size is:", unsafe.Sizeof(min64), "bytes")
	fmt.Println("=== END type int ===")
}

func typeUint() {
	fmt.Println("=== START type uint ===")
	var numberUint8 uint8 = 1 << 1
	var number2Uint8 uint8 = 1 >> 1
	fmt.Println("left shift uint8:", numberUint8)
	fmt.Println("left shift uint8:", number2Uint8)
	numberUint8 = (1 << 8) - 1
	fmt.Println("uint8 max value:", numberUint8, "size:", unsafe.Sizeof(numberUint8), "bytes")
	var numberUint16 uint16 = (1 << 16) - 1
	fmt.Println("uint16 max value:", numberUint16, "size:", unsafe.Sizeof(numberUint16), "bytes")
	var numberUint32 uint32 = (1 << 32) - 1
	fmt.Println("uint32 max value:", numberUint32, "size:", unsafe.Sizeof(numberUint32), "bytes")
	var numberUint64 uint64 = (1 << 64) - 1
	fmt.Println("uint64 max value:", numberUint64, "size:", unsafe.Sizeof(numberUint64), "bytes")
	fmt.Println("=== END type uint ===")
}
