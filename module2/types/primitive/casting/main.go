package main

import "fmt"

func main() {
	var numberInt int
	numberInt = 3
	var numberFloat float32
	numberFloat = float32(numberInt)
	fmt.Printf("тип: %T, значение: %v\n", numberFloat, numberFloat)

	var numberFloat32 float32
	numberFloat32 = 3.3
	var numberInt1 int
	numberInt1 = int(numberFloat32)
	fmt.Printf("тип: %T, значение: %v", numberInt1, numberInt1)

}
