package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 112358132134
	fmt.Println("size is:", unsafe.Sizeof(n), "bytes")
	typeUint()
}

func typeUint() {
	fmt.Println("=== START type int")
	var uintNumber uint8 = 1 << 7
	var min = int8(uintNumber)
	uintNumber--
	var max = int8(uintNumber)
	fmt.Println(min, max)
	fmt.Println("int 8 min value:", min, "int 8 max value:", max, "size is:", unsafe.Sizeof(min), "bytes")

	var uintNumber16 uint16 = 1 << 15
	var min16 = int16(uintNumber16)
	uintNumber16--
	var max16 = int16(uintNumber16)
	fmt.Println(min16, max16)
	fmt.Println("int 16 min value:", min16, "int 16 max value:", max16, "size is:", unsafe.Sizeof(min16), "bytes")

	var uintNumber32 uint32 = 1 << 31
	var min32 = int32(uintNumber32)
	uintNumber32--
	var max32 = int32(uintNumber32)
	fmt.Println(min32, max32)
	fmt.Println("int 32 min value:", min32, "int 32 max value:", max32, "size is:", unsafe.Sizeof(min32), "bytes")

	var uintNumber64 uint64 = 1 << 63
	var min64 = int64(uintNumber64)
	uintNumber64--
	var max64 = int64(uintNumber64)
	fmt.Println(min64, max64)
	fmt.Println("int 64 min value:", min64, "int 64 max value:", max64, "size is:", unsafe.Sizeof(min64), "bytes")
	fmt.Println("=== END type int")
}
