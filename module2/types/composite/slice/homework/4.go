//Измени программу так, чтобы изменения
//во втором слайсе не влияли на первый слайс

package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func main() {
	users := []User{
		{
			Name: "Eva",
			Age:  13,
		},
		{
			Name: "Victor",
			Age:  28,
		},
		{
			Name: "Dex",
			Age:  34,
		},
		{
			Name: "Billy",
			Age:  21,
		},
		{
			Name: "Foster",
			Age:  29,
		},
	}
	newslice := make([]User, 5)
	copy(newslice, users)
	subUsers := newslice[2:len(users)]
	editSecondSlice(subUsers)
	fmt.Println(users, subUsers)
}

func editSecondSlice(users []User) {
	for i := range users {
		users[i].Name = "unknown"
	}
}

//5 часов делать задачу - что только не пробуя
//а такое простое решение оказалось ...
//но процесс приятный)
//и надеюсь результат верный
