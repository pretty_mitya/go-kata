/*
Удали пользователей возрастом
выше 40 лет из слайса, выведи
результат в консоль
*/
package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func main() {
	users := []User{
		{
			Name: "Alice",
			Age:  21,
		},
		{
			Name: "John",
			Age:  34,
		},
		{
			Name: "Alexander",
			Age:  45,
		},
		{
			Name: "Ivan",
			Age:  13,
		},
		{
			Name: "Denis",
			Age:  44,
		},
		{
			Name: "Mary",
			Age:  26,
		},
		{
			Name: "Rose",
			Age:  41,
		},
	}
	n := 0
	for _, correct := range users {
		if correct.Age < 40 {
			users[n] = correct
			n++
		}
	}
	users = users[:n]
	fmt.Println(users)
}

//users = append(users, x)
//users = append(users[:2], users[3:]...)

//users = removeByIndex(users, 2)
//users1 := users[0:2]
//users2 := users[3:4]
//users3 := users[5:6]
//	func remove(s []int, i int) []int {
//		s[i] = s[len(s)-1]
//		return s[:len(s)-1]
//	}
//func removeByIndex(array []string, index int) []string {
//	return append(array[:index], array[index+1:]...)
//}
