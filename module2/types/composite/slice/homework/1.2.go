/*
Исправь функцию Append, чтобы изменения
повлияли на массив в функции main, решение
должно вывести четыре значения в консоль
*/
package main

import (
	"fmt"
)

func main() {
	s := []int{1, 2, 3}
	s = Append(s)
	fmt.Println(s)
}

func Append(s []int) []int {
	s = append(s, 4)
	return s
}
