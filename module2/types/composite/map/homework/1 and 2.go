// You can edit this code!
// Click here and start typing.
package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "https://github.com/docker/roadmap",
			Stars: 1200,
		},
		{
			Name:  "https://github.com/docker/awesome-compose",
			Stars: 20600,
		},
		{
			Name:  "https://github.com/docker/build-push-action",
			Stars: 3000,
		},
		{
			Name:  "https://github.com/docker/index-cli-plugin",
			Stars: 36,
		},
		{
			Name:  "https://github.com/docker/extensions-sdk",
			Stars: 95,
		},
		{
			Name:  "https://github.com/docker/docs",
			Stars: 3700,
		},
		{
			Name:  "https://github.com/docker/for-mac",
			Stars: 2300,
		},
		{
			Name:  "https://github.com/docker/buildx",
			Stars: 2300,
		},
		{
			Name:  "https://github.com/docker/metadata-action",
			Stars: 520,
		},
		{
			Name:  "https://github.com/docker/bake-action",
			Stars: 86,
		},
		{
			Name:  "https://github.com/docker/getting-started",
			Stars: 2400,
		},
		{
			Name:  "https://github.com/docker/node-sdk",
			Stars: 100,
		},
		// сюда впишите ваши остальные 12 структур
	}

	for i, maps := range projects {
		mapprojects := map[string]int{
			maps.Name: maps.Stars,
		}
		fmt.Println(i+1, mapprojects)
	}
}

// в цикле запишите в map
// в цикле пройдитесь по мапе и выведите значения в консоль

// если верно понял, то 2 задание:
// "Выведи в цикле значения карты, используя range"
// сделал также тут
