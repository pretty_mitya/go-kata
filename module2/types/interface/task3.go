// Исправь 37 строку программы,
// чтобы распечатать имена пользователей.
// Внимание: можно удалить 37 строку,
// дописать рабочий код, не модифицируя
// функцию testUserName
package main

import (
	"fmt"
)

var _ Userer = &User{}

type User struct {
	ID   int
	Name string
}

func (u *User) GetName() string {
	return u.Name
}

type Userer interface {
	GetName() string
}

func main() {
	u := []*User{
		{
			ID:   34,
			Name: "Annet",
		},
		{
			ID:   55,
			Name: "John",
		},
		{
			ID:   89,
			Name: "Alex",
		},
	}
	users := []Userer{}
	for _, h := range u {
		users = append(users, h)
	}
	// я это делал больше 8 часов - но это кайф найти решение!!!
	// надеюсь, верно
	testUserName(users)

}

func testUserName(users []Userer) {
	for _, u := range users {
		fmt.Println(u.GetName())
	}
}
