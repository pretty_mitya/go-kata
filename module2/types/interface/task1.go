// Исправь код так, чтобы
// программа вывела текст Success!.
// Для решения задачи нужно
// использовать type-switch.
// В функции test надо определить,
// является ли интерфейсное значение nil.
package main

import (
	"fmt"
)

type MyInterface interface{}

func main() {
	var n *int
	fmt.Println(n == nil)
	//var nn MyInterface
	//nn = n
	//fmt.Println(nn == nil)
	test(n)
	//fmt.Printf("%T %v", n, n)
}

func test(r interface{}) {
	switch r.(type) {
	case nil: // если и дата, и тип == nil - interface == nil
		fmt.Println("interface data && type == nil")
	default: // success будет если тип не nil( если имеет значение nil, то уже тип определен)
		fmt.Println("Success!")
	}
}
