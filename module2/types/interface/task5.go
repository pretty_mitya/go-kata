// You can edit this code!
// Click here and start typing.
package main

import "fmt"

type User struct {
	ID   int
	Name string
}

func (u User) GetName(username string) string {
	return u.Name
}

type Userer interface {
	GetName(username string) string
}

func main() {
	var i Userer
	i = User{}
	_ = i
	fmt.Println("Success!")
}

// если все правильно понял, код не компилировался
// из-за того, что не был имплементирован User
// потому что разнился getname в методе и интерфейсе
