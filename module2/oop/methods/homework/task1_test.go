package main

import (
	"fmt"
	"testing"
)

func TestOperations(t *testing.T) {

	var testsofsum = []struct {
		a, b   float64
		result float64
	}{
		{
			a:      3,
			b:      1,
			result: 4,
		}, {
			a:      4,
			b:      2,
			result: 6,
		}, {
			a:      6,
			b:      3,
			result: 9,
		}, {
			a:      7,
			b:      4,
			result: 11,
		}, {
			a:      8,
			b:      4,
			result: 12,
		}, {
			a:      9,
			b:      3,
			result: 12,
		}, {
			a:      10,
			b:      2,
			result: 12,
		}, {
			a:      12,
			b:      6,
			result: 18,
		},
	}
	for i, slice := range testsofsum {
		calc := NewCalc()
		res := calc.SetA(slice.a).SetB(slice.b).Do(sum).Result()
		fmt.Println(i+1, "example:", slice.a, "+", slice.b, "=", sum(slice.a, slice.b), "or res =", res)

		res2 := calc.Result()
		fmt.Println(res2)

	}

	testsofdivide := []struct {
		a, b   float64
		result float64
	}{
		{
			a:      3,
			b:      1,
			result: 3,
		}, {
			a:      4,
			b:      2,
			result: 2,
		}, {
			a:      6,
			b:      3,
			result: 2,
		}, {
			a:      7,
			b:      4,
			result: 3,
		}, {
			a:      8,
			b:      4,
			result: 2,
		}, {
			a:      9,
			b:      3,
			result: 3,
		}, {
			a:      10,
			b:      2,
			result: 5,
		}, {
			a:      12,
			b:      6,
			result: 2,
		},
	}
	for i, slice := range testsofdivide {
		calc := NewCalc()
		res := calc.SetA(slice.a).SetB(slice.b).Do(divide).Result()
		fmt.Println(i+1, "example:", slice.a, "/", slice.b, "=", divide(slice.a, slice.b), "or res =", res)

		res2 := calc.Result()
		fmt.Println(res2)
	}

	testsofaverage := []struct {
		a, b   float64
		result float64
	}{
		{
			a:      3,
			b:      1,
			result: 2,
		}, {
			a:      4,
			b:      2,
			result: 3,
		}, {
			a:      6,
			b:      3,
			result: 4,
		}, {
			a:      7,
			b:      4,
			result: 5,
		}, {
			a:      8,
			b:      4,
			result: 6,
		}, {
			a:      9,
			b:      3,
			result: 6,
		}, {
			a:      10,
			b:      2,
			result: 6,
		}, {
			a:      12,
			b:      6,
			result: 9,
		},
	}
	for i, slice := range testsofaverage {
		calc := NewCalc()
		res := calc.SetA(slice.a).SetB(slice.b).Do(average).Result()
		fmt.Println(i+1, "example:", slice.a, "+", slice.b, "/2 =", average(slice.a, slice.b), "or res =", res)

		res2 := calc.Result()
		fmt.Println(res2)
	}

	testsofmultiply := []struct {
		a, b   float64
		result float64
	}{
		{
			a:      3,
			b:      1,
			result: 3,
		}, {
			a:      4,
			b:      2,
			result: 8,
		}, {
			a:      6,
			b:      3,
			result: 18,
		}, {
			a:      7,
			b:      4,
			result: 28,
		}, {
			a:      8,
			b:      4,
			result: 32,
		}, {
			a:      9,
			b:      3,
			result: 27,
		}, {
			a:      10,
			b:      2,
			result: 20,
		}, {
			a:      12,
			b:      6,
			result: 72,
		},
	}
	for i, slice := range testsofmultiply {
		calc := NewCalc()
		res := calc.SetA(slice.a).SetB(slice.b).Do(multiply).Result()
		fmt.Println(i+1, "example:", slice.a, "*", slice.b, "=", multiply(slice.a, slice.b), "or res =", res)

		res2 := calc.Result()
		fmt.Println(res2)
	}
}
