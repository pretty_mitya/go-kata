package main

import (
	"errors"
	"fmt"
)

// MergeDictsJob is a job to merge dictionaries into a single dictionary.
type MergeDictsJob struct {
	Dicts      []map[string]string
	Merged     map[string]string
	IsFinished bool
}

// errors
var (
	errNotEnoughDicts = errors.New("at least 2 dictionaries are required")
	errNilDict        = errors.New("nil dictionary")
)

// BEGIN (write your solution here)

func (i *MergeDictsJob) truestatement() {
	i.IsFinished = true
}

func ExecuteMergeDictsJob(job *MergeDictsJob) (*MergeDictsJob, error) {
	job.Merged = make(map[string]string)
	if len(job.Dicts) < 2 {
		return job, errNotEnoughDicts
	}
	for _, i := range job.Dicts {
		if i == nil {
			return job, errNilDict
		}
		for key, value := range i {
			job.Merged[key] = value
		}
	}
	defer job.truestatement()
	return job, nil
}

func main() {

	example := []map[string]string{{"key1": "value1"}}
	mergejob := &MergeDictsJob{
		Dicts:      example,
		Merged:     nil,
		IsFinished: true,
	}
	_, errorss := ExecuteMergeDictsJob(mergejob)
	fmt.Println(mergejob.IsFinished, mergejob.Merged, errorss)

}

//func main() {
//	ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"}, nil}})
//}

// END

// Пример работы
// ExecuteMergeDictsJob(&MergeDictsJob{}) // &MergeDictsJob{IsFinished: true}, "at least 2 dictionaries are required"
// ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"},nil}}) // &MergeDictsJob{IsFinished: true, Dicts: []map[string]string{{"a": "b"},nil}}, "nil dictionary"
// ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"},{"b": "c"}}}) // &MergeDictsJob{IsFinished: true, Dicts: []map[string]string{{"a": "b", "b": "c"}}}, nil

// над этой задачей мозг взрывается больше, чем на интерфейсами

// гугл не помогает, а то, что появляется - снизу коммит прикрепил...

//GOROOT=/usr/local/go #gosetup
//GOPATH=/Users/mityaastrakhantsev/go #gosetup
///usr/local/go/bin/go build -o /private/var/folders/mj/sq2m28jx1vd0l1gx5bm74vww0000gn/T/GoLand/___go_build_task1_1_go /Users/mityaastrakhantsev/go/src/gitlab.com/pretty_mitya/go-kata/module2/functions/defer/homework/task1.1.go #gosetup
///private/var/folders/mj/sq2m28jx1vd0l1gx5bm74vww0000gn/T/GoLand/___go_build_task1_1_go
