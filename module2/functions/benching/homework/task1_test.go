package main

import (
	"testing"
)

func Benchmark_MapUserProducts(b *testing.B) {
	a := genUsers()
	c := genProducts()
	for i := 0; i < b.N; i++ {
		MapUserProducts(a, c)
	}

}

func Benchmark_MapUserProducts2(b *testing.B) {
	a := genUsers()
	c := genProducts()
	for i := 0; i < b.N; i++ {
		MapUserProducts2(a, c)
	}
}
