package main

import (
	"testing"
)

func Benchmark_SanitizeText(b *testing.B) {
	ft := NewFilterText()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for i := range data {
			ft.SanitizeText(data[i])
		}
	}
}
func Benchmark_SanitizeText2(b *testing.B) {
	ft := NewFilterText()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for i := range data {
			ft.SanitizeText2(data[i])
		}
	}
}
