package main

import (
	"testing"
)

var (
	pets Pets
	err  error
	data []byte
)

func BenchmarkUnmarshalPetsJson(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		pets, err = UnmarshalPets(jsonData)
		if err != nil {
			panic(err)
		}
	}
}

func BenchmarkMarshalPetsJson(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		data, err = pets.Marshal()
		if err != nil {
			panic(err)
		}
	}
}

func BenchmarkUnmarshal2PetsJson(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		pets, err = UnmarshalPets2(jsonData)
		if err != nil {
			panic(err)
		}
	}
}

func BenchmarkMarshal2PetsJson(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		data, err = pets.Marshal2()
		if err != nil {
			panic(err)
		}
	}
}
