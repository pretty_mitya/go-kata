// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse and unparse this JSON data, add this code to your project and do:
//
//    pets, err := UnmarshalPets(bytes)
//    bytes, err = pets.Marshal()

package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	jsoniter "github.com/json-iterator/go"
)

var jsonData []byte = []byte(` {
    "id": 9223372036854764000,
    "category": {
      "id": 1,
      "name": "animal007"
    },
    "name": "Bulldog",
    "photoUrls": [
      "https://site.com/bulldog/photo"
    ],
    "tags": [
      {
        "id": 1,
        "name": "#dog"
      }
    ],
    "status": "pending"
  }`)

type Pets Pet

func UnmarshalPets(data []byte) (Pets, error) {
	var r Pets
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Pets) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

func UnmarshalPets2(data []byte) (Pets, error) {
	var r Pets
	err := jsoniter.Unmarshal(data, &r)
	return r, err
}

func (r *Pets) Marshal2() ([]byte, error) {
	return jsoniter.Marshal(r)
}

type Pet struct {
	ID        float64    `json:"id"`
	Category  *Category  `json:"category,omitempty"`
	Name      string     `json:"name"`
	PhotoUrls []string   `json:"photoUrls"`
	Tags      []Category `json:"tags"`
	Status    string     `json:"status"`
}

type Category struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

func main() {
	jsonFile, err := os.Open("task1.json")
	if err != nil {
		fmt.Println(err)
	}
	defer jsonFile.Close()
	_, err = ioutil.ReadAll(jsonFile)
	if err != nil {
		fmt.Println(err)
	}
}
