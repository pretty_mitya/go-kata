package main

import (
	"fmt"
	"sync"

	"golang.org/x/sync/errgroup"
)

type Cache struct {
	mx   sync.Mutex
	data map[string]interface{}
	init bool
}

func NewCache() *Cache {
	return &Cache{
		data: make(map[string]interface{}, 100),
		init: true,
	}
}

func (c *Cache) Set(key string, v interface{}) error {
	c.mx.Lock()
	defer c.mx.Unlock()
	if !c.init {
		return fmt.Errorf("cache isnt initialized")
	}
	c.data[key] = v

	return nil
}

func (c *Cache) Get(key string) interface{} {
	c.mx.Lock()
	defer c.mx.Unlock()
	if !c.init {
		return nil
	}
	return c.data[key]
}

func main() {
	cache := NewCache()
	keys := []string{
		"programming",
		"is",
		"so",
		"awesome",
		"write",
		"clean",
		"code",
		"use",
		"solid",
		"principles",
	}
	//var mu sync.RWMutex
	var eg errgroup.Group
	for i := range keys {
		//mu.RLock()
		idx := i
		eg.Go(func() error {
			return cache.Set(keys[idx], idx)
		})
		//mu.RUnlock()
	}

	err := eg.Wait()
	if err != nil {
		panic(err)
	}
}
