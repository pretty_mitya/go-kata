package main

import (
	"context"
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func joinChannels(chs ...<-chan int) chan int {
	mergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}

		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for id := range ch {
					mergedCh <- id
				}
			}(ch, wg)
		}

		wg.Wait()
		close(mergedCh)
	}()

	return mergedCh
}

func generateData() chan int {
	out := make(chan int, 1000)

	go func() {
		defer close(out)
		for {
			select {
			case _, ok := <-out:
				if !ok {
					return
				}
			case <-time.After(1 * time.Second):
				out <- rand.Intn(100)
			}
		}
	}()
	return out
}

func main() {
	rand.Seed(time.Now().UnixNano())
	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	out := generateData()

	//ticker := time.NewTicker(5 * time.Second)
	tickerChan := make(chan bool)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	go func() {
		for num := range out {
			a <- num
		}
	}()

	go func() {
		for num := range out {
			b <- num
		}
	}()

	go func() {
		for num := range out {
			c <- num
		}
	}()

	go func() {
		for {
			select {
			case <-tickerChan:
				return
			case <-ctx.Done():
				close(a)
				close(b)
				close(c)
				tickerChan <- true
			}
		}
	}()

	go func() {
		select {
		case <-ctx.Done():
		case <-time.After(time.Second * 5):
		}
	}()

	mainChan := joinChannels(a, b, c)
	for num := range mainChan {
		fmt.Println(num)
	}

	fmt.Println("Ticker is turned off!")
}
