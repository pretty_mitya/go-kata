package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func joinChannels(chs ...<-chan int) chan int {
	mergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}

		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for id := range ch {
					mergedCh <- id
				}
			}(ch, wg)
		}

		wg.Wait()
		close(mergedCh)
	}()

	return mergedCh
}

func generateData() chan int {
	out := make(chan int, 1000)

	go func() {
		defer close(out)
		for {
			select {
			case _, ok := <-out:
				if !ok {
					return
				}
			case <-time.After(1 * time.Second):
				out <- rand.Intn(100)
			}
		}
	}()
	return out
}

func main() {
	rand.Seed(time.Now().UnixNano())
	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	out := generateData()

	ticker := time.NewTicker(30 * time.Second)
	tickerChan := make(chan bool)

	go func() {
		for num := range out {
			a <- num
		}
		fmt.Println("completed1")
	}()

	go func() {
		for num := range out {
			b <- num
		}
		fmt.Println("completed2")
	}()

	go func() {
		for num := range out {
			c <- num
		}
		fmt.Println("completed3")
	}()

	go func() {
		for {
			select {
			case <-tickerChan:
				return
			case tm := <-ticker.C:
				fmt.Println("--->The Current time is: ", tm)
				close(a)
				close(b)
				close(c)
				fmt.Println("-----------------------")
				tickerChan <- true
			}
		}
	}()

	mainChan := joinChannels(a, b, c)
	for num := range mainChan {
		fmt.Println(num)
	}

	fmt.Println("Ticker is turned off!")
}
